<x-layout-kcd>
    <style type="text/css">
	.container input[type="submit"], .back{
		padding: 15px 30px;
		float: right;
	}
	.invalid{
		/* background: rgba(255, 25, 60, 0.4); */
		color: red;
		padding: 5px;
		border-radius: 10px;
		margin-top: 5px;
		text-transform: uppercase;
		font-size: 14px;
	}
	</style>
	<form action="kecamatan/sekolah/import" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="input-group" style="float: left; width: 400px;">
                            <input required type="file" class="form-control" id="inputGroupFile04" required
                                aria-describedby="inputGroupFileAddon04" aria-label="Upload" name="excel">
                            <button class="btn btn-outline-secondary" type="submit"
                                id="inputGroupFileAddon04">Submit</button>
                        </div>
                    </form>
    <div class="container-index">
        @include('map.map')
    </div> 
    <script>
        $(document).ready(function(){

            $(".leaflet-interactive")
					.mousedown(function (evt) {
							isDragging = false;
							startingPos = [evt.pageX, evt.pageY];
					})
					.mousemove(function (evt) {
							if (!(evt.pageX === startingPos[0] && evt.pageY === startingPos[1])) {
									isDragging = true;
							}
					})
					.mouseup(function () {
							if (isDragging) {
									console.log("Drag");
							} else {
									console.log("Click");
									$('#school').modal('show');
									console.log($(this).index()+1);
									var id = $(this).index()+1;
									window.location.href = "/kcd/kecamatan/"+id+"/sekolah";
							}
							isDragging = false;
							startingPos = [];
					});
        });
    </script>
</x-layout-kcd>
