<x-layout-kcd>
    <main>
        <div class="card">
            <div class="card-body">
                <div class="">
                    <h2 align="center">DATA SEKOLAH KECAMATAN {{ $kecamatan->nama_kecamatan }}</h2>
                    {{-- <a type="button" style="float: right;" class="btn btn-success fa fa-plus" href="input-sekolah.php"></a> --}}
                    <a href="../../kecamatan" type="button" class="btn btn-warning fa fa-chevron-left"></a>
                    <button style="float: right;" class="btn btn-success fa fa-plus" data-target=".bd-example-modal-xl"
                    data-bs-toggle="modal" data-bs-target=".form"></button>
                    @include('form.input-sekolah')
                </div>
                    <table border="1" class="table table-bordered" style="border-collapse: collapse; margin-top: 25px;">
                    <tr align="center" class="table-primary">
                        <th>NO</th>
                        <th>NAMA SEKOLAH</th>
                        <th>NPSN</th>
                        <th>NSS</th>
                        <th>OPSI</th>
                        <th>ACTION</th>
                        
                    </tr>
                    @if($data->isEmpty())
                        <tr align="center">
                            <td colspan="5">KOSONG</td>
                        </tr>
                    @else
                        @php $no = 1 @endphp
                            @foreach( $data as $tampil )
                                <tr>
                                    <td align="center">{{ $no++ }}</td>
                                    <td>{{ $tampil->nama }}</td>
                                    <td>{{ $tampil->npsn }}</td>
                                    <td>{{ $tampil->nss }}</td>
                                    <td align="center"><a href="../../sekolah/{{ $tampil->id }}/profil" class="btn btn-primary">Profil</a>
                                    <a href="/kcd/sekolah/{{ $tampil->id }}/surat-rekomendasi" class="btn btn-primary">Surat Rekomendasi</a></td>
                                    <td align="center"><a href="/kcd/sekolah/{{ $tampil->id }}/hapus" class="btn btn-danger fa fa-trash" onclick="return confirm('Anda yakin ingin menghapus sekolah ini?')"></a></td>
                                </tr>
                            @endforeach
                        @endif
                    {{-- <tr align="center">
                        <td><b>JUMLAH</b></td>
                        <td colspan="4"></td>
			        </tr> --}}
                </table>
            </div>
        </div>
    </main>
</x-layout-kcd>
