<div class="modal fade" id="school" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Login</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="container">
                    <form action="sekolah/sessions" method="post">
                        @csrf
                        <div class="row">
                            <div class="col-25">
                                <label for="sekolah">Kecamatan</label>
                            </div>
                            <div class="col-75">
                                <input type="text" name="" id="kecamatan" value="" disabled>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-25">
                                <label for="sekolah">Sekolah</label>
                            </div>
                            <div class="col-75">
                                <select name="sekolah" id="sekolah" required></select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-25">
                                <label for="username">Username</label>
                            </div>
                            <div class="col-75">
                                <input type="text" name="username" autocomplete="off" class="login" id="username"
                                    required="">
                                <div class="invalid"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-25">
                                <label for="password">Password</label>
                            </div>
                            <div class="col-75">
                                <input type="password" name="password" autocomplete="off" class="login" id="pass"
                                    required="">
                                <div class="invalid"></div>
                            </div>
                        </div>
                        <input type="hidden" name="sekolah_id" id="sekolah_id">
                        <div class="modal-footer" style="margin-top: 15px;">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                            <button type="submit" id="btn-login" class="btn btn-primary" name="submit-sekolah" disabled>Login</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
