<x-layout-form-edit>
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="myExtraLargeModalLabel">Edit Siswa</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <div class="container">
                <form action="siswa/update" method="post">
                    @csrf
                    @method('PATCH')
                    <input type="hidden" name="id" id="siswa-edit-id" value="">
                    <div class="row">
                        <div class="col-25">
                            <label>Jurusan/Keahlian</label>
                        </div>
                        <div class="col-75">
                            <input type="text" disabled id="siswa-edit-jurusan">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="kelas_10">Rombel Kelas X</label>
                        </div>
                        <div class="col-75">
                            <input type="number" name="kelas_10" id="siswa-edit-x" step="false" value="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="kelas_11">Rombel Kelas XI</label>
                        </div>
                        <div class="col-75">
                            <input type="number" name="kelas_11" id="siswa-edit-xi" step="false" value="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="kelas_12">Rombel Kelas XII</label>
                        </div>
                        <div class="col-75">
                            <input type="number" name="kelas_12" id="siswa-edit-xii" step="false"
                                value="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="kelas_13">Rombel Kelas XIII</label>
                        </div>
                        <div class="col-75">
                            <input type="number" name="kelas_13" id="siswa-edit-xiii" step="false"
                                value="">
                        </div>
                        <div class="row">
                            <input type="submit" name="submit" value="submit">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</x-layout-form-edit>
