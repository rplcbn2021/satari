<x-layout-form>
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="myExtraLargeModalLabel">Input Siswa Berprestasi</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <div class="container">
                <form action="siswa-prestasi/create" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-25">
                            <label for="nama">Nama Lengkap</label>
                        </div>
                        <div class="col-75">
                            <input type="text" name="nama">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="nis">NIS</label>
                        </div>
                        <div class="col-75">
                            <input type="text" name="nis">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="kelas">Kelas</label>
                        </div>
                        <div class="col-75">
                            <input type="text" name="kelas">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="prestasi_siswa">Prestasi Yang Diperoleh</label>
                        </div>
                        <div class="col-75">
                            <input type="text" name="prestasi_siswa">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="event">Event Yang Diikuti</label>
                        </div>
                        <div class="col-75">
                            <input type="text" name="event">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="tanggal_event">Tahun Penyelenggaraan</label>
                        </div>
                        <div class="col-75">
                            <input type="date" name="tanggal_event">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="tingkat_id">Tingkat</label>
                        </div>
                        <div class="col-75">
                            <select name="tingkat_id">
                                <option hidden>Pilih Tingkat</option>
                                @foreach( $tingkat as $pilihan )

                                    <option value="{{ $pilihan->id }}">{{ $pilihan->nama_tingkat }}</option>

                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <input type="submit" name="submit" value="submit">
                    </div>
                </form>
            </div>
        </div>
    </div>
</x-layout-form>
