<x-layout-form-edit>
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="myExtraLargeModalLabel">Edit PTK</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <div class="container">

                <form action="ptk/update" method="post">
                    @csrf
                    @method('PATCH')
                    <input type="hidden" id="ptk-edit-id" name="id" value="">
                    <div class="row">
                        <div class="col-25">
                            <label for="nip">NIP/NUPTK</label>
                        </div>
                        <div class="col-75">
                            <input type="text" name="nip" id="ptk-edit-nip" autocomplete="off" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="nama">Nama Lengkap</label>
                        </div>
                        <div class="col-75">
                            <input type="text" name="nama" autocomplete="off" id="ptk-edit-nama" required="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="tempat_lahir">Tempat Lahir</label>
                        </div>
                        <div class="col-75">
                            <input type="text" name="tempat_lahir" autocomplete="off" id="ptk-edit-tempat_lahir" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="tanggal_lahir">Tanggal Lahir</label>
                        </div>
                        <div class="col-75">
                            <input type="date" name="tanggal_lahir" id="ptk-edit-tanggal_lahir" max="" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="alamat">Alamat</label>
                        </div>
                        <div class="col-75">
                            <textarea name="alamat" style="height: 150px;" id="ptk-edit-alamat" required></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="telepon">Telepon/HP</label>
                        </div>
                        <div class="col-75">
                            <input type="text" name="telepon" id="ptk-edit-telepon"></div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="email">Email</label>
                        </div>
                        <div class="col-75">
                            <input type="email" name="email" id="ptk-edit-email">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-25">
                            <label for="status_pegawai_id">Status</label>
                        </div>
                        <div class="col-75">
                            <select name="status_pegawai_id" id="ptk-edit-status_pegawai_id" required>
                                <option value="" hidden>Pilih Status</option>
                                @foreach($status as $result)

                                    <option value="{{ $result->id }}">{{ $result->status }}</option>

                                @endforeach

                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="pendidikan_id">Pendidikan</label>
                        </div>
                        <div class="col-75">
                            <select name="pendidikan_id" id="ptk-edit-pendidikan_id" required>
                                <option value="" hidden>Pilih Pendidikan</option>
                                @foreach($pendidikan as $result)

                                    <option value="{{ $result->id }}">{{ $result->pendidikan }}</option>

                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="jabatan_id">Jabatan</label>
                        </div>
                        <div class="col-75">
                            <select name="jabatan_id" id="ptk-edit-jabatan_id" required>
                                <option value="" hidden>Pilih Jabatan</option>
                                @foreach($jabatan as $result)

                                    <option value="{{ $result->id }}">{{ $result->jabatan }}</option>

                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <input type="submit" name="submit" value="submit">
                    </div>
                </form>
            </div>
        </div>

    </div>
  </x-layout-form-edit>
