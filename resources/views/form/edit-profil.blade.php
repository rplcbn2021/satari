<x-layout-form>
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="myExtraLargeModalLabel">Edit Sekolah</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <div class="container">

                <form action="profil/update" method="post">
                    @csrf
                    @method('PATCH')
                    <div class="row">
                        <div class="col-30">
                            <label for="npsn">NPSN</label>
                        </div>
                        <div class="col-70">
                            <input type="text" name="npsn" value="{{ $data->npsn }}" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-30">
                            <label for="nss">NSS</label>
                        </div>
                        <div class="col-70">
                            <input type="text" name="nss" value="{{ $data->nss }}">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-30">
                            <label for="nama">Nama Sekolah</label>
                        </div>
                        <div class="col-70">
                            <input type="text" name="nama" value="{{ $data->nama }}" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-30">
                            <label for="alamat">Alamat</label>
                        </div>
                        <div class="col-70">
                            <textarea name="alamat" style="height: 200px;">{{ $data->alamat }}</textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-30">
                            <label for="telepon">Telepon</label>
                        </div>
                        <div class="col-70"><input type="text" name="telepon" value="{{ $data->telepon }}"></div>
                    </div>
                    <div class="row">
                        <div class="col-30">
                            <label for="fax">Fax</label>
                        </div>
                        <div class="col-70"><input type="text" name="fax" value="{{ $data->fax }}"></div>
                    </div>
                    <div class="row">
                        <div class="col-30">
                            <label for="email">Email</label>
                        </div>
                        <div class="col-70">
                            <input type="email" name="email" value="{{ $data->email }}" required>
                        </div>
                    </div>
                    <div class="row">
                        <input type="submit" name="submit" value="submit">
                    </div>
                </form>
            </div>
        </div>

    </div>
</x-layout-form>
