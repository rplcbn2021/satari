<x-layout-form>
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="myExtraLargeModalLabel">Input Rombel</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <div class="container">
                <form action="rombel/create" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-25">
                            <label for="jurusan_id">Jurusan/Keahlian</label>
                        </div>
                        <div class="col-75">
                            <input type="text" list="jurusan" name="jurusan_id" autocomplete="off">
                            <datalist name="jurusan" id="jurusan">
                                @foreach( $jurusan as $pilihan )
                                    <option>{{ $pilihan->nama_jurusan }}</option>
                                @endforeach
                                </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="kelas_10">Rombel Kelas X</label>
                        </div>
                        <div class="col-75">
                            <input type="number" name="kelas_10" id="kelas_10" step="false">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="kelas_11">Rombel Kelas XI</label>
                        </div>
                        <div class="col-75">
                            <input type="number" name="kelas_11" id="kelas_11" step="false">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="kelas_12">Rombel Kelas XII</label>
                        </div>
                        <div class="col-75">
                            <input type="number" name="kelas_12" id="kelas_12" step="false">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="kelas_13">Rombel Kelas XIII</label>
                        </div>
                        <div class="col-75">
                            <input type="number" name="kelas_13" id="kelas_13" step="false">
                        </div>
                    </div>
                    {{-- <div class="row">
                <div class="col-25">
                    <label for="total">Total Rombel</label>
                </div>
                <div class="col-75">
                    <input type="number" name="total" value="" disabled id="total">
                </div>
            </div> --}}
                    <div class="row">
                        <input type="submit" name="submit" value="submit">
                </form>
            </div>
        </div>
    </div>
</x-layout-form>
