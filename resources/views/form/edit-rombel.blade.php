<x-layout-form-edit>
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="myExtraLargeModalLabel">Edit Rombel</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <div class="container">
                <form action="rombel/update" method="post">
                    @csrf
                    @method('PATCH')
                    <input type="hidden" name="id" id="rombel-edit-id" value="">
                    <div class="row">
                        <div class="col-25">
                            <label for="jurusan_id">Jurusan/Keahlian</label>
                        </div>
                        <div class="col-75">
                            <input type="text" id="rombel-edit-jurusan" disabled list="jurusan" name="jurusan_id" autocomplete="off">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="kelas_10">Rombel Kelas X</label>
                        </div>
                        <div class="col-75">
                            <input type="number" name="kelas_10" id="rombel-edit-x" step="false">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="kelas_11">Rombel Kelas XI</label>
                        </div>
                        <div class="col-75">
                            <input type="number" name="kelas_11" id="rombel-edit-xi" step="false">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="kelas_12">Rombel Kelas XII</label>
                        </div>
                        <div class="col-75">
                            <input type="number" name="kelas_12" id="rombel-edit-xii" step="false">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="kelas_13">Rombel Kelas XIII</label>
                        </div>
                        <div class="col-75">
                            <input type="number" name="kelas_13" id="rombel-edit-xiii" step="false">
                        </div>
                    </div>
                    {{-- <div class="row">
                <div class="col-25">
                    <label for="total">Total Rombel</label>
                </div>
                <div class="col-75">
                    <input type="number" name="total" value="" disabled id="total">
                </div>
            </div> --}}
                    <div class="row">
                        <input type="submit" name="submit" value="submit">
                </form>
            </div>
        </div>
    </div>
</x-layout-form-edit>
