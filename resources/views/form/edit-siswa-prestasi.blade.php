<x-layout-form-edit>
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="myExtraLargeModalLabel">Edit Siswa Berprestasi</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <div class="container">
                <form action="siswa-prestasi/update" method="post">
                    @csrf
                    @method('PATCH')
                    <input type="hidden" name="id" id="siswa-prestasi-edit-id">
                    <div class="row">
                        <div class="col-25">
                            <label for="nama">Nama Lengkap</label>
                        </div>
                        <div class="col-75">
                            <input type="text" name="nama" id="siswa-prestasi-edit-nama">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="nis">NIS</label>
                        </div>
                        <div class="col-75">
                            <input type="text" name="nis" id="siswa-prestasi-edit-nis">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="kelas">Kelas</label>
                        </div>
                        <div class="col-75">
                            <input type="text" name="kelas" id="siswa-prestasi-edit-kelas">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="prestasi_siswa">Prestasi Yang Diperoleh</label>
                        </div>
                        <div class="col-75">
                            <input type="text" name="prestasi_siswa" id="siswa-prestasi-edit-prestasi">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="event">Event Yang Diikuti</label>
                        </div>
                        <div class="col-75">
                            <input type="text" name="event" id="siswa-prestasi-edit-event">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="tanggal_event">Tahun Penyelenggaraan</label>
                        </div>
                        <div class="col-75">
                            <input type="date" name="tanggal_event" id="siswa-prestasi-edit-tanggal-event">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="tingkat_id">Tingkat</label>
                        </div>
                        <div class="col-75">
                            <select name="tingkat_id" id="siswa-prestasi-edit-tingkat">
                                <option value="" hidden>Pilih Tingkat</option>
                                @foreach( $tingkat as $pilihan )

                                    <option value="{{ $pilihan->id }}">{{ $pilihan->nama_tingkat }}</option>

                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <input type="submit" name="submit" value="submit">
                    </div>
            </div>
        </div>
    </div>
</x-layout-form-edit>
