<x-layout-form-edit>
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="myExtraLargeModalLabel">Edit Sarpras</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <div class="container">
                <form action="sapras/update" method="post">
                    @csrf
                    @method('PATCH')
                    <input type="hidden" name="id" id="sapras-edit-id">
                    <div class="row">
                        <div class="col-25">
                            <label for="ruang">Ruang</label>
                        </div>
                        <div class="col-75">
                            <input type="text" name="ruang" id="sapras-edit-ruang">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="nama">Nama Barang</label>
                        </div>
                        <div class="col-75">
                            <input type="text" name="nama" id="sapras-edit-nama">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="kondisi_baik">Jumlah Kondisi Baik</label>
                        </div>
                        <div class="col-75">
                            <input type="number" name="kondisi_baik" id="sapras-edit-baik">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="kondisi_sedang">Jumlah Rusak Sedang</label>
                        </div>
                        <div class="col-75">
                            <input type="number" name="kondisi_sedang" id="sapras-edit-sedang">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="kondisi_berat">Jumlah Rusak Berat</label>
                        </div>
                        <div class="col-75">
                            <input type="number" name="kondisi_berat" id="sapras-edit-berat">
                        </div>
                    </div>
                    <div class="row">
                        <input type="submit" name="submit" value="submit">
                    </div>
                </form>
            </div>
        </div>
    </div>
</x-layout-form-edit>