<x-layout-form-edit>
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="myExtraLargeModalLabel">Edit Mapel</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <div class="container">

                <form action="mapel/update" method="post">
                    @csrf
                    @method('PATCH')
                    <div class="row row-na">
                      <input type="hidden" name="id" id="mapel-edit-id" value="">
                        <div class="col-25">
                            <label for="mapel">Mata Pelajaran</label>
                        </div>
                        <div class="col-75">
                            <input type="text" name="" list="mapel" value="" id="mapel-edit-nama" autocomplete="off" disabled>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="jam_kelas_x">Total Jam Kelas 10</label>
                        </div>
                        <div class="col-75">
                            <input type="number" name="jam_kelas_x" id="mapel-edit-x" value="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="jam_kelas_xi">Total Jam Kelas 11</label>
                        </div>
                        <div class="col-75">
                            <input type="number" name="jam_kelas_xi" id="mapel-edit-xi" value="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="jam_kelas_xii">Total Jam Kelas 12</label>
                        </div>
                        <div class="col-75">
                            <input type="number" name="jam_kelas_xii" id="mapel-edit-xii" value="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="jam_kelas_xiii">Total Jam Kelas 13</label>
                        </div>
                        <div class="col-75">
                            <input type="number" name="jam_kelas_xiii" id="mapel-edit-xiii" value="">
                        </div>
                    </div>
                    <div class="row">
                        <input type="submit" name="submit" value="submit">
                    </div>
                </form>
            </div>
        </div>

    </div>
</x-layout-form-edit>
