<x-layout-form-edit>
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="myExtraLargeModalLabel">Edit Guru</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <div class="container">

                <form action="guru/update" method="post">
                    @csrf
                    @method('PATCH')
                    <input type="hidden" name="id" id="guru-edit-id" value="">
                    <div class="row">
                        <div class="col-25">
                            <label for="nip">NIP/NUPTK</label>
                        </div>
                        <div class="col-75">
                            <input type="text" name="nip" id="guru-edit-nip" required="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="nama">Nama Lengkap</label>
                        </div>
                        <div class="col-75">
                            <input type="text" name="nama" id="guru-edit-nama" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="tempat_lahir">Tempat Lahir</label>
                        </div>
                        <div class="col-75">
                            <input type="text" name="tempat_lahir" id="guru-edit-tempat-lahir" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="tanggal_lahir">Tanggal Lahir</label>
                        </div>
                        <div class="col-75">
                            <input type="date" name="tanggal_lahir" id="guru-edit-tanggal-lahir" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="alamat">Alamat</label>
                        </div>
                        <div class="col-75">
                            <textarea name="alamat" style="height: 150px;" id="guru-edit-alamat" required></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="telepon">Telepon/HP</label>
                        </div>
                        <div class="col-75">
                            <input type="text" name="telepon" id="guru-edit-telepon">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="email">Email</label>
                        </div>
                        <div class="col-75">
                            <input type="email" name="email" id="guru-edit-email">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="status_pegawai_id">Status Kepegawaian</label>
                        </div>
                        <div class="col-75">
                            <select name="status_pegawai_id" required id="guru-edit-status-pegawai">
                                <option value="" hidden>Pilih Status</option>
                                @foreach( $status as $pilihan )

                                    <option value="{{ $pilihan->id }}">{{ $pilihan->status }}</option>

                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="pendidikan_id">Pendidikan</label>
                        </div>
                        <div class="col-75">
                            <select name="pendidikan_id" required id="guru-edit-pendidikan">
                                <option value="" hidden>Pilih Pendidikan</option>
                                @foreach( $pendidikan as $pilihan )

                                    <option value="{{ $pilihan->id }}">{{ $pilihan->pendidikan }}</option>

                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row row-na">
                        <div class="col-25">
                            <label for="mapel_id">Mata Pelajaran</label>
                        </div>
                        <div class="col-75">
                            <select name="mapel_id" required id="guru-edit-mapel">
                                <option value="" hidden>Pilih Mata Pelajaran</option>
                                @foreach( $mapel as $pilihan )

                                    <option value="{{ $pilihan->id }}">{{ $pilihan->nama_mapel }}</option>

                                @endforeach


                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <input type="submit" name="submit" value="submit">
                    </div>
                </form>
            </div>
        </div>

    </div>
</x-layout-form-edit>
