<html>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Surat Rekomendasi</title>
    <style>
        .container{
            width: 19cm;
            margin: auto;
            padding: 10px;
        }
        table{
            width: 100%;
        }
    </style>
</head>

<body>
    <div class="container">
    <table>
        <tr>
            <td><img src="" width="90" height="90" alt="avatar"></td>
            <td>
                <center>
                    <font size="4">PEMERINTAH DAERAH PROVINSI JAWA BARAT</font><br>
                    <font size="4">DINAS PENDIDIKAN</font><br>
                    <font size="4">CABANG DINAS PENDIDIKAN WILAYAH I</font><br>
                    <font size="2">Jl.Karadenan No. 7 Cibinong, Bogor Telepon. Fax.</font><br>
                    <font size="2">Email: website :</font><br>
                    <font size="2">CIBINONG-16913</font><br>
                </center>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <hr>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td>
                <b>SURAT REKOMENDASI</b>
            <td>Nomor:</td>
            </td>
        </tr>
    </table>
    <br>
    <br>
    <table>
        <tr>
            <td>
                <p>
                    Berdasarkan hasil rekaman pendataan sekolah melalui aplikasi Satuan Data Mandiri KCD Wilayah 1
                    Provinsi Jawa Barat, maka berikut kami lampirkan rekomendasi untuk sekolah
                    <!-- nama sekolah --> dengan rincian sebagai berikut :
                </p>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td>1.Data Pendidik Mapel Umum</td>
        </tr>
    </table>
    <table border="1" style="border-collapse: collapse;">
        <tr>
            <td>No</td>
            <td>Mata Pelajaran</td>
            <td>Jumlah Guru</td>
            <td>Kebutuhan</td>
            <td>Rekomendasi</td>
        </tr>
        @if ($jam->where('mapel.blok', 1)->isEmpty())
            <tr>
                <td colspan="5">TIDAK ADA DATA</td>
            </tr>
        @else
        <?php $no = 1; ?>
        @foreach ( $jam->where('mapel.blok', 1) as $tampil )
            <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $tampil->mapel->nama_mapel }}</td>
                <td>{{ $guru = $tampil->mapel->guru->count() }}</td>
                <td>{{ $quota = $tampil->tingkat_kebutuhan->quota }}</td>
                <td>@php
                    if ($guru == 0){
                        $guru = -1;
                    }
                    switch ($guru) {

                        case $guru <= $quota:
                            echo 'Ditambah'; 
                            break;

                        case $guru > $quota:
                            echo 'Dipindahkan';
                            break;
                        
                        default:
                            echo 'Sesuai kebutuhan';
                            break;
                    }
                @endphp
                </td>
            </tr>
        @endforeach
        @endif
    </table>
    <table>
        <tr>
            <td>2.Data Pendidik Mapel Produktif</td>
        </tr>
    </table>
    <table border="1" style="border-collapse: collapse;">
        <tr>
            <td>No</td>
            <td>Kompetensi Keahlian</td>
            <td>Mata Pelajaran</td>
            <td>Jumlah Guru</td>
            <td>Kebutuhan</td>
            <td>Rekomendasi</td>
        </tr>
        @if ($jam->where('mapel.blok', 2)->isEmpty())
            <tr>
                <td colspan="5">TIDAK ADA DATA</td>
            </tr>
        @else
        <?php $no = 1; ?>
        @foreach ( $jam->where('mapel.blok', 2) as $tampil )
            <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $tampil->mapel->nama_mapel }}</td>
                <td>{{ $tampil->mapel->jurusan->nama_jurusan }}</td>
                <td>{{ $guru = $tampil->mapel->guru->count() }}</td>
                <td>{{ $quota = $tampil->tingkat_kebutuhan->quota }}</td>
                <td>@php
                    if ($guru == 0){
                        $guru = -1;
                    }
                    switch ($guru) {

                        case $guru <= $quota:
                            echo 'Ditambah'; 
                            break;

                        case $guru > $quota:
                            echo 'Dipindahkan';
                            break;
                        
                        default:
                            echo 'Sesuai kebutuhan';
                            break;
                    }
                @endphp
                </td>
            </tr>
        @endforeach
        @endif
    </table>
    {{-- <table>
        <tr>
            <td>
                <b>Keterangan</b>
            </td>
        </tr>
    </table>
    @php
        $kebutuhan = $tingkat_kebutuhan->sum('lebih') - $tingkat_kebutuhan->sum('kurang')? $tingkat_kebutuhan->sum('lebih') - $tingkat_kebutuhan->sum('kurang') : 0;
    @endphp --}}
    {{-- <table>
        <tr>
            <td><input type="checkbox" {{ $kebutuhan > 0? "checked": ""; }}></td>
            <td>Mutasi</td>
        </tr>
        <tr>
            <td><input type="checkbox" {{ $kebutuhan < 0? "checked": ""; }}></td>
            <td>Ditambah</td>
        </tr>
        <tr>
            <td><input type="checkbox" {{ $kebutuhan == 0? "checked": ""; }}></td>
            <td>Tidak Ada</td>
        </tr>
    </table><br> --}}
    <table width="625">
        <tr>
            <td>3.Data Pendidik Berprestasi</td>
        </tr>
    </table>
    
    <table border="1" style="border-collapse: collapse;">
        <tr>
            <td>No</td>
            <td>Nama Pendidik</td>
            <td>Mata Pelajaran Yang Diampu</td>
            <td>Prestasi</td>
            <td>Rekomendasi</td>
        </tr>
         @if ($guru_prestasi->isEmpty())
            <tr>
                <td colspan="5">TIDAK ADA DATA</td>
            </tr>
        @else
        <?php $no = 1 ?>
        @foreach ( $guru_prestasi as $tampil )
            <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $tampil->guru->nama }}</td>
                <td>{{ $tampil->guru->mapel->nama_mapel }}</td>
                <td>{{ $tampil->prestasi_guru }}</td>
                <td>Nama tersebut agar dapat diikutsertakan dalam pendidikan dan pelatihan tingkat selanjutnya.</td>
            </tr>
        @endforeach
        @endif
    </table><br>
    {{-- <table>
        <tr>
            <td>3.Data Tenaga Kependidikan</td>
        </tr>
    </table>
    <table border="1" style="border-collapse: collapse;">
        <tr>
            <td>No</td>
            <td>Jumlah</td>
            <td>Kebutuhan</td>
            <td>Rekomendasi</td>
        </tr>
         @if ($guru_prestasi->isEmpty())
            <tr>
                <td colspan="5">TIDAK ADA DATA</td>
            </tr>
        @else
        @foreach ( $guru_prestasi as $tampil )
            <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $tampil->guru->nama }}</td>
                <td>{{ $tampil->guru->mapel->nama_mapel }}</td>
                <td>{{ $tampil->prestasi_guru }}</td>
                <td>Nama tersebut agar dapat diikutsertakan dalam pendidikan dan pelatihan tingkat selanjutnya.</td>
            </tr>
        @endforeach
        @endif
    </table><br> --}}
    {{-- <table>
        <tr>
            <td>4.Data Tenaga Kependidikan Berprestasi</td>
        </tr>
    </table>
    
    <table border="1" style="border-collapse: collapse;">
        <tr>
            <td>No</td>
            <td>Nama Tenaga Kependidikan</td>
            <td>Tempat Bertugas</td>
            <td>Prestasi</td>
            <td>Rekomendasi</td>
        </tr>
    </table><br> --}}
    <table>
        <tr>
            <td>4.Data Siswa Berprestasi</td>
        </tr>
    </table>
    <table border="1" style="border-collapse: collapse;">
        <tr>
            <td>No</td>
            <td>Nama Siswa</td>
            <td>Kelas / Kompetensi Keahlian</td>
            <td>Prestasi</td>
            <td>Rekomendasi</td>
        </tr>
    @if ($siswa_prestasi->isEmpty())
            <tr>
                <td colspan="5">TIDAK ADA DATA</td>
            </tr>
    @else
    <?php $no = 1 ?>
    @foreach ($siswa_prestasi as $tampil )
        <tr>
            <td>{{ $no++ }}</td>
            <td>{{ $tampil->nama }}</td>
            <td>{{ $tampil->kelas }}</td>
            <td>{{ $tampil->prestasi_siswa }}</td>
            <td>Nama tersebut agar dapat diikutsertakan dalam pendidikan dan pelatihan tingkat selanjutnya.</td>
        </tr>
    @endforeach
    @endif
    </table><br>
    <table>
        <tr>
            <td>5.Data Sarana dan Prasarana</td>
        </tr>
    </table>
    <table border="1" style="border-collapse: collapse;">
        <tr>
            <td>No</td>
            <td>Nama Barang</td>
            <td>Jumlah Kondisi Rusak Berat</td>
            <td>Rekomendasi</td>
        </tr>
    @if ($sapras->isEmpty())
        <tr>
            <td colspan="5">TIDAK ADA DATA</td>
        </tr>
    @else
    <?php $no = 1 ?>
    @foreach ($sapras->where('kondisi_berat', '>', 0) as $tampil )
        <tr>
            <td>{{ $no++ }}</td>
            <td>{{ $tampil->nama }}</td>
            <td>{{ $tampil->kondisi_berat }}</td>
            <td></td>
        </tr>
    @endforeach
    @endif
    </table><br>
    <table>
        <tr>
            <td>6.Data Siswa Per Rombel Per Kompetensi Keahlian</td>
        </tr>
    </table>
    <table border="1" style="border-collapse: collapse;">
        <tr>
            <td>No</td>
            <td>Kompetensi Keahlian</td>
            <td>Jumlah Siswa</td>
            <td>Jumlah Rombel</td>
            <td>Rekomendasi</td>
        </tr>
        @if ($rombel->isEmpty())
        <tr>
            <td colspan="5">TIDAK ADA DATA</td>
        </tr>
        @else
        <?php $no = 1 ?>
        @foreach ($rombel as $tampil )
            @php
                $total_siswa = 
                    $tampil->siswa->first()->kelas_10 +
                    $tampil->siswa->first()->kelas_11 +
                    $tampil->siswa->first()->kelas_12 +
                    $tampil->siswa->first()->kelas_13;
                $total_rombel = 
                    $tampil->kelas_10 +
                    $tampil->kelas_11 +        
                    $tampil->kelas_12 +        
                    $tampil->kelas_13; 
            @endphp
            <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $tampil->jurusan->nama_jurusan }}</td>
                <td>{{ $total_siswa }}</td>
                <td>{{ $total_rombel }}</td>
                <td>
                    @switch(true)
                        @case($total_siswa/$total_rombel <= 5)
                                {{ 'tutup' }}
                            @break
                        @case($total_siswa/$total_rombel <= 10)
                                {{ 'merger' }}
                            @break
                        @case($total_siswa/$total_rombel <= 20)
                                {{ 'promosi' }}
                            @break
                        @case($total_siswa/$total_rombel <= 30)
                                {{ 'pembinaan' }}
                            @break
                    
                        @default
                            {{ 'tambah rombel' }}
                    @endswitch
                </td>
            </tr>
        @endforeach
        @endif
    </table><br>
    <table>
        <tr>
            <td>
                <p>Demikian Rekomendasi ini dibuat berdasarkan data rekam Aplikasi Satuan data Mandiri KCD Wilayah 1
                    Dinas Pendidikan Provinsi Jawa Barat.</p>
            </td>
        </tr>
    </table>
    </div>
</body>
</head>

</html>
