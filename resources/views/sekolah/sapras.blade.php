<x-layout-sekolah>
    <main class="" style="margin-top: 7px; ">
        <div class="card">
            <div class="card-body">
                <h2 align="center">DATA SAPRAS</h2>
                <div class="" id="form-display">
                    <div class="">
                        <div class="input">
                            @if(auth()->user()->can('operator_sekolah'))
                                <form method="post" action="sapras/import" enctype="multipart/form-data">
                                    @csrf
                                    <div class="input-group" style="float: left; width: 400px;">
                                        <input required type="file" class="form-control" id="inputGroupFile04" required
                                            aria-describedby="inputGroupFileAddon04" aria-label="Upload" name="excel">
                                        <button class="btn btn-outline-secondary" type="submit"
                                            id="inputGroupFileAddon04">Submit</button>
                                    </div>
                                </form>
                                <button type="button" style="float: right;" data-target=".bd-example-modal-xl" data-bs-toggle="modal" data-bs-target=".form" class="btn btn-success fa fa-plus"></button>
                                @include('form.input-sapras')
                            @endif
                            {{-- <input style="float: left; margin-bottom:8px;" type="text" placeholder="Cari Data..."
                            id="search"> --}}
                        </div>
                    </div>
                    <table border="1" class="table table-bordered" style="border-collapse: collapse; margin-top: 5px;"
                        id="table">
                        <tr align="center" class="table-primary">
                            <th rowspan="2">NO</th>
                            <th rowspan="2">RUANGAN</th>
                            <th rowspan="2">BARANG</th>
                            <th colspan="3">KONDISI</th>
                            <th rowspan="2">JUMLAH</th>
                            @if(auth()->user()->can('operator_sekolah'))
                                <th rowspan="2" colspan="2">OPSI</th>
                            @endif
                        </tr>
                        <tr align="center" class="table-primary">
                            <th>BAIK</th>
                            <th>RUSAK SEDANG</th>
                            <th>RUSAK BERAT</th>
                        </tr>
                        @if($data->isEmpty())
                            <tr align="center">
                                <td colspan="8">KOSONG</td>
                            </tr>
                        @else
                            @php 
                                $no = 1; 
                                $jumlah = 0;
                            @endphp
                                @foreach($data as $tampil)
                                    <tr>
                                        <input type="hidden" class="id" value="{{ $tampil->id }}">
                                        <td>{{ $no++ }}</td>
                                        <td>{{ $tampil->ruang }}</td>
                                        <td>{{ $tampil->nama }}</td>
                                        <td align="center">{{ $baik = $tampil->kondisi_baik }}</td>
                                        <td align="center">{{ $sedang = $tampil->kondisi_sedang }}</td>
                                        <td align="center">{{ $berat = $tampil->kondisi_berat }}</td>
                                        <td align="center">{{ $total = $baik + $sedang + $berat }}</td>
                                        @php $jumlah += $total @endphp
                                        @if(auth()->user()->can('operator_sekolah'))
                                            <td align="center"><button class="btn btn-warning fa fa-pencil edit" data-target=".bd-example-modal-xl" data-bs-toggle="modal" data-bs-target=".form-edit"></button></td>
                                            <td align="center"><a href="sapras/{{ $tampil->id }}/hapus"
                                                    class="btn btn-danger fa fa-trash"
                                                    onclick="return confirm('Anda yakin ingin menghapus data ini?')"></a>
                                            </td>
                                        @endif
                                    </tr>

                                @endforeach
                                <tr align="center">
                                    <td colspan="3">Total</td>
                                    <td>{{ $tampil->sum('kondisi_baik') }}</td>
                                    <td>{{ $tampil->sum('kondisi_sedang') }}</td>
                                    <td>{{ $tampil->sum('kondisi_berat') }}</td>
                                    <td>{{ $jumlah }}</td>
                                    <td colspan="2"></td>
                                </tr>
                                @include('form.edit-sapras')
                            @endif
                    </table>
                </div>
            </div>
        </div>
    </main>

    <div class="footer">
        <div class="row">
            <span style="color : white">RPL CIBIONE &copy; 2021</span>
        </div>
    </div>

    <script>
        $(document).ready(function () {

            $('.edit').click(function(){
                var id = $(this).parent().siblings('.id').val();
                var data = {!! json_encode($data) !!}.filter(data=> data.id == id);
                
                $('#sapras-edit-id').val(data[0].id);
                $('#sapras-edit-ruang').val(data[0].ruang);
                $('#sapras-edit-nama').val(data[0].nama);
                $('#sapras-edit-baik').val(data[0].kondisi_baik);
                $('#sapras-edit-sedang').val(data[0].kondisi_sedang);
                $('#sapras-edit-berat').val(data[0].kondisi_berat);
            
            });
        });

    </script>

    </body>

    </html>
</x-layout-sekolah>
