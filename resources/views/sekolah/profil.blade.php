<x-layout-sekolah>
    <main class="" style="margin-top: 7px; ">
        <div class="card">
            <div class="card-body">
                <h2 align="center">PROFIL SEKOLAH
                    @if(auth()->user()->can('operator_sekolah'))
                        <button class="btn btn-warning fa fa-pencil" data-target=".bd-example-modal-xl" data-bs-toggle="modal" data-bs-target=".form"></button>
                        @include('form.edit-profil')
                    @endif
                </h2>
                
                <div class="row" style="margin-top: 40px;">
                    <div class="col">NPSN</div>
                    <div class="col">:&emsp;{{ $data->npsn }}</div>
                </div>
                <div class="row">
                    <div class="col">NSS</div>
                    <div class="col">:&emsp;{{ $data->nss }}</div>
                </div>
                <div class="row">
                    <div class="col">Nama Sekolah</div>
                    <div class="col">:&emsp;{{ $data->nama }}</div>
                </div>
                <div class="row">
                    <div class="col">Alamat</div>
                    <div class="col">:&emsp;{{ $data->alamat }}</div>
                </div>
                <div class="row">
                    <div class="col">Telepon/Fax</div>
                    <div class="col">:&emsp;{{ $data->telepon }} / {{ $data->fax }}</div>
                </div>
                <div class="row">
                    <div class="col">Email</div>
                    <div class="col">:&emsp;{{ $data->email }}</div>
                </div>
                <div class="accordion" id="accordionExample" style="margin-top: 50px;">
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingOne">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                Rombel
                            </button>
                        </h2>
                        <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne"
                            data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <table border="1" class="table table-bordered"
                                    style="border-collapse: collapse; margin-top: 5px;" id="table">
                                    <tr>
                                        <td>Kelas 10</td>
                                        <td>{{ $x = $data->rombel->sum('kelas_10') }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Kelas 11</td>
                                        <td>{{ $xi = $data->rombel->sum('kelas_11') }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Kelas 12</td>
                                        <td>{{ $xii = $data->rombel->sum('kelas_12') }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Kelas 13</td>
                                        <td>{{ $xiii = $data->rombel->sum('kelas_13') }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Total</td>
                                        <td>{{ $x + $xi + $xii + $xiii }}</td>
                                    </tr>
                                </table>
                                <a href="rombel" class="btn btn-primary">Rombel</a>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingTwo">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Siswa
                            </button>
                        </h2>
                        <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo"
                            data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <table border="1" class="table table-bordered"
                                    style="border-collapse: collapse; margin-top: 5px;" id="table">
                                    <tr>
                                        <td>Kelas 10</td>
                                        <td>{{ $x = $data->siswa->sum('kelas_10') }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Kelas 11</td>
                                        <td>{{ $xi = $data->siswa->sum('kelas_11') }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Kelas 12</td>
                                        <td>{{ $xii = $data->siswa->sum('kelas_12') }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Kelas 13</td>
                                        <td>{{ $xiii = $data->siswa->sum('kelas_13') }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Total</td>
                                        <td>{{ $x + $xi + $xii + $xiii }}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingThree">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Guru
                            </button>
                        </h2>
                        <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree"
                            data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <table border="1" class="table table-bordered"
                                    style="border-collapse: collapse; margin-top: 5px;" id="table">
                                    <tr class="table-primary">
                                        <th>Mapel</th>
                                        <th>Jumlah Guru</th>
                                        <th>Quota</th>
                                        <th>Lebih</th>
                                        <th>Kurang</th>
                                    </tr>
                                    @foreach( $data->jam as $tampil)
                                        <tr>
                                            <td>{{ $tampil->mapel->nama_mapel }}</td>
                                            <td>{{ $tampil->mapel->guru->count() }}</td>
                                            <td>{{ $tampil->tingkat_kebutuhan->quota }}</td>
                                            <td>{{ $tampil->tingkat_kebutuhan->lebih }}</td>
                                            <td>{{ $tampil->tingkat_kebutuhan->kurang }}</td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td>Total</td>
                                        <td>{{ $data->guru->count() }}</td>
                                        <td>{{ $data->tingkat_kebutuhan->sum('quota') }}</td>
                                        <td>{{ $data->tingkat_kebutuhan->sum('lebih') }}</td>
                                        <td>{{ $data->tingkat_kebutuhan->sum('kurang') }}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingFour">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                Guru Berprestasi
                            </button>
                        </h2>
                        <div id="collapseFour" class="accordion-collapse collapse" aria-labelledby="headingFour"
                            data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <table border="1" class="table table-bordered"
                                    style="border-collapse: collapse; margin-top: 5px;" id="table">
                                    <tr>
                                        <td>Kecamatan</td>
                                        <td>{{ $data->guru_prestasi->where('tingkat_id', 1)->count() }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Kabupaten</td>
                                        <td>{{ $data->guru_prestasi->where('tingkat_id', 2)->count() }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Provinsi</td>
                                        <td>{{ $data->guru_prestasi->where('tingkat_id', 3)->count() }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Nasional</td>
                                        <td>{{ $data->guru_prestasi->where('tingkat_id', 4)->count() }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Internasional</td>
                                        <td>{{ $data->guru_prestasi->where('tingkat_id', 5)->count() }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Total</td>
                                        <td>{{ $data->guru_prestasi->count() }}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingFive">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                data-bs-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                PTK
                            </button>
                        </h2>
                        <div id="collapseFive" class="accordion-collapse collapse" aria-labelledby="headingFive"
                            data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <table border="1" class="table table-bordered"
                                    style="border-collapse: collapse; margin-top: 5px;" id="table">
                                    <tr>
                                        <td>Total</td>
                                        <td>{{ $data->ptk->count() }}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingSix">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                data-bs-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                Sarpras
                            </button>
                        </h2>
                        <div id="collapseSix" class="accordion-collapse collapse" aria-labelledby="headingSix"
                            data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <table border="1" class="table table-bordered"
                                    style="border-collapse: collapse; margin-top: 5px;" id="table">
                                    <tr>
                                        <td>Kondisi Baik</td>
                                        <td>{{ $bb = $data->sapras->sum('kondisi_baik') }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Kondisi Sedang</td>
                                        <td>{{ $bs = $data->sapras->sum('kondisi_sedang') }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Kondisi Rusak</td>
                                        <td>{{ $br = $data->sapras->sum('kondisi_rusak') }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Total</td>
                                        <td>{{ $bb + $bs + $br }}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </main>




    <div class="footer">
        <div class="row">
            <span style="color : white">RPL CIBIONE &copy; 2021</span>
        </div>
    </div>

    </body>

    </html>
</x-layout-sekolah>
