<x-layout-sekolah>
    <main class="" style="margin-top: 7px; ">
        <div class="card">
            <div class="card-body">
                <h2 align="center">DATA GURU</h2>
                <div class="" id="form-display">
                    <div class="">
                        <div class="input">
                            @if(auth()->user()->can('operator_sekolah'))
                                <form method="post" action="guru/import" enctype="multipart/form-data">
                                    @csrf
                                    <div class="input-group" style="float: left; width: 400px;">
                                        <input required type="file" class="form-control" id="inputGroupFile04" required
                                            aria-describedby="inputGroupFileAddon04" aria-label="Upload" name="excel">
                                        <button class="btn btn-outline-secondary" type="submit"
                                            id="inputGroupFileAddon04">Submit</button>
                                    </div>
                                </form>
                                <button style="float: right;" class="btn btn-success fa fa-plus" data-target=".bd-example-modal-xl"
                                    data-bs-toggle="modal" data-bs-target=".form"></button>
                                    @include('form.input-guru')
                            @endif
                            {{-- <input style="float: left; margin-bottom:8px;" type="text" placeholder="Cari Data..."
                            id="search"> --}}
                        </div>
                    </div>
                    <table border="1" class="table table-bordered" style="border-collapse: collapse; margin-top: 5px;"
                        id="table">
                        <tr align="center" class="table-primary">
                            <th>No</th>
                            <th>Nama</th>
                            <th>NIP</th>
                            <th>Pendidikan</th>
                            <th>Status Kepegawaian</th>
                            <th>Mata Pelajaran</th>
                            @if(auth()->user()->can('operator_sekolah'))
                                <th colspan="2">Opsi</th>
                            @endif
                        </tr>
                        @if($data->isEmpty())
                            <tr align="center">
                                <td colspan="9">KOSONG</td>
                            </tr>
                        @else
                            @php $no = 1; @endphp
                                @foreach($data as $tampil)
                                    <tr>
                                        <input type="hidden" value="{{ $tampil->id }}" class="id">
                                        <td>{{ $no++ }}</td>
                                        <td>{{ $tampil->nama }}</td>
                                        <td>{{ $tampil->nip }}</td>
                                        <td>{{ $tampil->pendidikan->pendidikan }}</td>
                                        <td>{{ $tampil->status_pegawai->status }}</td>
                                        <td>{{ $tampil->mapel->nama_mapel }}</td>
                                        @if(auth()->user()->can('operator_sekolah'))
                                            <td align="center"><a href="guru/{{ $tampil->id }}/edit"
                                                    class="btn btn-warning fa fa-pencil edit" data-target=".bd-example-modal-xl" data-bs-toggle="modal"
                                                data-bs-target=".form-edit"></a></td>
                                            <td align="center"><a href="guru/{{ $tampil->id }}/hapus"
                                                    class="btn btn-danger fa fa-trash"
                                                    onclick="return confirm('Anda yakin ingin menghapus data ini?')"></a>
                                            </td>
                                        @endif
                                    </tr>

                                @endforeach
                                @if (auth()->user()->can('operator_sekolah'))
                                    @include('form.edit-guru')
                                @endif
                            @endif
                    </table>

                    <table border="1" class="table table-bordered" style="width: 50px;">
                        <tr class="table-primary">
                            <th>Mapel</th>
                            <th>Jumlah Guru</th>
                            <th>Total Jam</th>
                            <th>Quota</th>
                            <th>Lebih</th>
                            <th>Kurang</th>
                        </tr>
                        @foreach($jam as $tampil)
                            <tr>
                                <td>{{ $tampil->mapel->nama_mapel }}</td>
                                <td
                                    {{ $guru = $tampil->mapel->guru->count() != 0? "class='belum-input'": "" }}>
                                    {!! $guru = $tampil->mapel->guru->count() == 0? 0: $tampil->mapel->guru->count() !!}</td>
                                <td>{{ $tampil->total_jam? $tampil->total_jam : 0 }}</td>
                                <td>{{ $tampil->tingkat_kebutuhan->quota? $tampil->tingkat_kebutuhan->quota : 0 }}</td>
                                <td>{{ $tampil->tingkat_kebutuhan->lebih }}</td>
                                <td>{{ $tampil->tingkat_kebutuhan->kurang }}</td>
                            </tr>

                        @endforeach
                        <tr>
                            <td>Total</td>
                            <td colspan="5"></td>
                        </tr>
                    </table>
                </div>
                <script>
                $(document).ready(function () {

                    $('.edit').click(function(){
                        var id = $(this).parent().siblings('.id').val();
                        var data = {!! json_encode($data) !!}.filter(data=> data.id == id);
                    
                        $('#guru-edit-id').val(data[0].id);
                        $('#guru-edit-nip').val(data[0].nip);
                        $('#guru-edit-nama').val(data[0].nama);
                        $('#guru-edit-tempat-lahir').val(data[0].tempat_lahir);
                        $('#guru-edit-tanggal-lahir').val(data[0].tanggal_lahir);
                        $('#guru-edit-alamat').val(data[0].alamat);
                        $('#guru-edit-telepon').val(data[0].telepon);
                        $('#guru-edit-email').val(data[0].email);
                        $("#guru-edit-status-pegawai").val(data[0].status_pegawai_id).change();
                        $("#guru-edit-pendidikan").val(data[0].pendidikan_id).change();
                        $("#guru-edit-mapel").val(data[0].mapel_id).change();
                    });
                });

            </script>
            </div>
        </div>
    </main>

    <div class="footer">
        <div class="row">
            <span style="color : white">RPL CIBIONE &copy; 2021</span>
        </div>
    </div>

    </body>

    </html>
</x-layout-sekolah>
