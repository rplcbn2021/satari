<x-layout-sekolah>
    <main class="" style="margin-top: 7px; ">
        <div class="card">
            <div class="card-body">
                <h2 align="center">DATA SISWA PRESTASI</h2>
                <div class="" id="form-display">
                    <div class="">
                    </div>
                    <div class="table-responsive" style="white-space: nowrap;">
                        <div class="input">
                            @if(auth()->user()->can('operator_sekolah'))
                                <button class="btn btn-success fa fa-plus" data-target=".bd-example-modal-xl"
                                    data-bs-toggle="modal" data-bs-target=".form"></button>
                                @include('form.input-siswa-prestasi')
                            @endif
                            {{-- <input style="float: left; margin-bottom:8px;" type="text" placeholder="Cari Data..."
                            id="search"> --}}
                        </div>
                    <table border="1" class="table table-bordered" style="border-collapse: collapse; margin-top: 5px;"
                        id="table">
                        <tr align="center" class="table-primary">
                            <th rowspan="2">NO</th>
                            <th rowspan="2">NAMA</th>
                            <th rowspan="2">NIS</th>
                            <th rowspan="2">KELAS</th>
                            <th rowspan="2">PRESTASI</th>
                            <th rowspan="2">EVENT</th>
                            <th rowspan="2">TAHUN EVENT</th>
                            <th colspan="5">TINGKAT</th>
                            @if(auth()->user()->can('operator_sekolah'))
                                <th rowspan="2" colspan="2">OPSI</th>
                            @endif
                        </tr>
                        <tr align="center" class="table-primary">
                            <th>KECAMATAN</th>
                            <th>KABUPATEN/KOTA</th>
                            <th>PROVINSI</th>
                            <th>NASIONAL</th>
                            <th>INTERNASIONAL</th>
                        </tr>
                        @if($data->isEmpty())
                            <tr align="center">
                                <td colspan="13">KOSONG</td>
                            </tr>
                        @else
                            @php
                                $no =1;
                            @endphp
                            @foreach($data as $tampil)
                                <tr>
                                    <input type="hidden" class="id" value="{{ $tampil->id }}">
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $tampil->nama }}</td>
                                    <td>{{ $tampil->nis }}</td>
                                    <td>{{ $tampil->kelas }}</td>
                                    <td>{{ $tampil->prestasi_siswa }}</td>
                                    <td>{{ $tampil->event }}</td>
                                    <td>{{ $tampil->tanggal_event }}</td>
                                    <td align="center">
                                        {{ $tampil->tingkat_prestasi->nama_tingkat == 'Kecamatan'? "√" : "" }}
                                    </td>
                                    <td align="center">
                                        {{ $tampil->tingkat_prestasi->nama_tingkat == 'Kabupaten/Kota'? "√" : "" }}
                                    </td>
                                    <td align="center">
                                        {{ $tampil->tingkat_prestasi->nama_tingkat == 'Provinsi'? "√" : "" }}
                                    </td>
                                    <td align="center">
                                        {{ $tampil->tingkat_prestasi->nama_tingkat == 'Nasional'? "√" : "" }}
                                    </td>
                                    <td align="center">
                                        {{ $tampil->tingkat_prestasi->nama_tingkat == 'Internasional'? "√" : "" }}
                                    </td>
                                    @if(auth()->user()->can('operator_sekolah'))
                                        <td align="center"><button class="btn btn-warning fa fa-pencil edit" data-target=".bd-example-modal-xl" data-bs-toggle="modal"
                                            data-bs-target=".form-edit"></a></td>
                                        <td align="center"><a href="siswa-prestasi/{{ $tampil->id }}/hapus"
                                                class="btn btn-danger fa fa-trash"
                                                onclick="return confirm('Anda yakin ingin menghapus data ini?')"></a>
                                        </td>
                                    @endif
                                </tr>

                            @endforeach
                            @if(auth()->user()->can('operator_sekolah'))
                                @include('form.edit-siswa-prestasi')
                            @endif
                        @endif
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <script>

        $(document).ready(function () {

            $('.edit').click(function () {
                var id = $(this).parent().siblings('.id').val();
                var data = {!!json_encode($data) !!}.filter(data => data.id == id);

                $('#siswa-prestasi-edit-id').val(data[0].id);
                $('#siswa-prestasi-edit-nis').val(data[0].nis);
                $('#siswa-prestasi-edit-nama').val(data[0].nama);
                $('#siswa-prestasi-edit-kelas').val(data[0].kelas);
                $('#siswa-prestasi-edit-prestasi').val(data[0].prestasi_siswa);
                $('#siswa-prestasi-edit-event').val(data[0].event);
                $('#siswa-prestasi-edit-tanggal-event').val(data[0].tanggal_event);
                $('#siswa-prestasi-edit-tingkat').val(data[0].tingkat_id).change();
            });
        });

    </script>
    <div class="footer">
        <div class="row">
            <span style="color : white">RPL CIBIONE &copy; 2021</span>
        </div>
    </div>

    </body>

    </html>
</x-layout-sekolah>
