<x-layout-sekolah>
    <main class="" style="margin-top: 7px; ">
        <div class="card">
            <div class="card-body">
                <h2 align="center">DATA SISWA</h2>
                <div class="" id="form-display">
                    <div class="">
                        <div class="input">
                            {{-- <input style="float: left; margin-bottom:8px;" type="text" placeholder="Cari Data..."
                            id="search"> --}}
                        </div>
                    </div>
                    <table border="1" class="table table-bordered" style="border-collapse: collapse; margin-top: 5px;"
                        id="table">
                        <tr align="center" class="table-primary">
                            <th rowspan="2">No</th>
                            <th rowspan="2">Jurusan</th>
                            <th colspan="4">Kelas</th>
                            <th rowspan="2">TOTAL</th>
                            @if(auth()->user()->can('operator_sekolah'))
                                <th rowspan="2" colspan="2">OPSI</th>
                            @endif
                        </tr>
                        <tr align="center" class="table-primary">
                            <th>X</th>
                            <th>XI</th>
                            <th>XII</th>
                            <th>XIII</th>
                        </tr>
                        @if($data->isEmpty())
                            <tr align="center">
                                <td colspan="8">KOSONG</td>
                            </tr>
                        @else
                            @php $no=1 @endphp
                                @foreach($data as $tampil)
                                    <tr>
                                        <input type="hidden" class="id" value="{{ $tampil->id }}">
                                        <td>{{ $no++ }}</td>
                                        <td class="jurusan">{{ $tampil->rombel->jurusan->nama_jurusan }}</td>
                                        <td align="center">{{ $kel10 = $tampil->kelas_10? $tampil->kelas_10 : 0 }}</td>
                                        <td align="center">{{ $kel11 = $tampil->kelas_11? $tampil->kelas_11 : 0 }}</td>
                                        <td align="center">{{ $kel12 = $tampil->kelas_12? $tampil->kelas_12 : 0 }}</td>
                                        <td align="center">{{ $kel13 = $tampil->kelas_13? $tampil->kelas_13 : 0 }}</td>
                                        <td align="center">{{ $kel10 + $kel11 + $kel12 + $kel13 }}</td>
                                        @if(auth()->user()->can('operator_sekolah'))
                                            <td align="center"><button class="btn btn-warning fa fa-pencil edit"  data-target=".bd-example-modal-xl" data-bs-toggle="modal" data-bs-target=".form-edit"></a></td>
                                        @endif
                                    </tr>
                                @endforeach
                            <tr align="center">
                                <td colspan="2">Total</td>
                                <td>{{ $tol10 = $data->sum('kelas_10') }}</td>
                                <td>{{ $tol11 = $data->sum('kelas_11') }}</td>
                                <td>{{ $tol12 = $data->sum('kelas_12') }}</td>
                                <td>{{ $tol13 = $data->sum('kelas_13') }}</td>
                                <td>{{ $tol10 + $tol11 + $tol12 + $tol13 }}</td>
                                <td></td>
                            </tr>
                            @if(auth()->user()->can('operator_sekolah'))
                                @include('form.edit-siswa')
                            @endif
                        @endif
                    </table>
            </div>
        </div>
        </div>
    </main>

    <div class="footer">
        <div class="row">
            <span style="color : white">RPL CIBIONE &copy; 2021</span>
        </div>
    </div>
    <script>
        $(document).ready(function () {

            $('.edit').click(function(){
                var id = $(this).parent().siblings('.id').val();
                var data = {!! json_encode($data) !!}.filter(data=> data.id == id);
                var jurusan = $('.jurusan').text();
                console.log(jurusan);
            
                $('#siswa-edit-id').val(data[0].id);
                $('#siswa-edit-jurusan').val(data[0].rombel.jurusan.nama_jurusan);
                $('#siswa-edit-x').val(data[0].kelas_10);
                $('#siswa-edit-xi').val(data[0].kelas_11);
                $('#siswa-edit-xii').val(data[0].kelas_12);
                $('#siswa-edit-xiii').val(data[0].kelas_13);
            });
        });
    </script>
    </body>

    </html>
</x-layout-sekolah>
