<x-layout-sekolah>
    <main class="" style="margin-top: 7px; ">
        <div class="card">
            <div class="card-body">
                <h2 align="center">DATA PTK</h2>
                <div class="" id="form-display">
                    <div class="">
                        <div class="input">
                            @if(auth()->user()->can('operator_sekolah'))
                                <form method="post" action="ptk/import" enctype="multipart/form-data">
                                    @csrf
                                    <div class="input-group" style="float: left; width: 400px;">
                                        <input required type="file" class="form-control" id="inputGroupFile04"
                                            aria-describedby="inputGroupFileAddon04" aria-label="Upload" name="excel">
                                        <button class="btn btn-outline-secondary" type="submit"
                                            id="inputGroupFileAddon04">Submit</button>
                                    </div>
                                </form>
                                <button type="button" style="float: right;" data-target=".bd-example-modal-xl" data-bs-toggle="modal" data-bs-target=".form" class="btn btn-success fa fa-plus"></button>
                                @include('form.input-ptk')
                                
                            @endif
                            {{-- <input style="float: left; margin-bottom:8px;" type="text" placeholder="Cari Data..."
                            id="search"> --}}
                        </div>
                    </div>
                    <table border="1" class="table table-bordered" style="border-collapse: collapse; margin-top: 5px;"
                        id="table">
                        <tr align="center" class="table-primary">
                            <th>No</th>
                            <th>Nama</th>
                            <th>NIP</th>
                            <th>Pendidikan</th>
                            <th>Status</th>
                            <th>Jabatan</th>
                            @if(auth()->user()->can('operator_sekolah'))
                                <th colspan="2">Opsi</th>
                            @endif
                        </tr>
                        @if($data->isEmpty())
                            <tr align="center">
                                <td colspan="7">KOSONG</td>
                            </tr>
                        @else
                            @php $no = 1; @endphp
                                @foreach($data as $tampil)
                                    <tr>
                                        <input type="hidden" class="id" value="{{ $tampil->id }}">
                                        <td>{{ $no++ }}</td>
                                        <td>{{ $tampil->nama }}</td>
                                        <td>{{ $tampil->nip }}</td>
                                        <td>{{ $tampil->pendidikan->pendidikan }}</td>
                                        <td>{{ $tampil->status_pegawai->status }}</td>
                                        <td>{{ $tampil->jabatan->jabatan }}</td>
                                        @if(auth()->user()->can('operator_sekolah'))
                                            <td align="center"><button class="btn btn-warning fa fa-pencil edit" data-target=".bd-example-modal-xl" data-bs-toggle="modal" data-bs-target=".form-edit"></button></td>
                                            <td align="center"><a href="ptk/{{ $tampil->id }}/hapus" class="btn btn-danger fa fa-trash" onclick="return confirm('Anda yakin ingin menghapus data ini?')"></a></td>
                                        @endif
                                    </tr>

                                @endforeach

                                @include('form.edit-ptk')
                            @endif
                    </table>

                    {{-- <table border="1" class="table table-bordered border-primary" style="width: 50px;">
                    <tr>
                        <th>Jumlah</th>
                        <th>Tingkat Kebutuhan</th>
                        <th>Lebih</th>
                        <th>Kurang</th>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </table> --}}
                </div>
                <script>
                $(document).ready(function () {

                    $('.edit').click(function(){
                        var id = $(this).parent().siblings('.id').val();
                        var data = {!! json_encode($data) !!}.filter(data=> data.id == id);
                        
                        $('#ptk-edit-id').val(data[0].id);
                        $('#ptk-edit-nip').val(data[0].nip);
                        $('#ptk-edit-nama').val(data[0].nama);
                        $('#ptk-edit-tempat_lahir').val(data[0].tempat_lahir);
                        $('#ptk-edit-tanggal_lahir').val(data[0].tanggal_lahir);
                        $('#ptk-edit-alamat').val(data[0].alamat);
                        $('#ptk-edit-telepon').val(data[0].telepon);
                        $('#ptk-edit-email').val(data[0].email);
                        $("#ptk-edit-status_pegawai_id").val(data[0].status_pegawai_id).change();
                        $("#ptk-edit-pendidikan_id").val(data[0].pendidikan_id).change();
                        $("#ptk-edit-jabatan_id").val(data[0].jabatan_id).change();
                    });
                });

            </script>
            </div>
        </div>
    </main>

    <div class="footer">
        <div class="row">
            <span style="color : white">RPL CIBIONE &copy; 2021</span>
        </div>
    </div>

    </body>

    </html>
</x-layout-sekolah>
