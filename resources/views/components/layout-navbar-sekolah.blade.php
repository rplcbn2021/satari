<div class="collapse navbar-collapse" id="navbarTogglerDemo02">
    <ul class="navbar-nav navbar-right me-auto mb-2 mb-lg-0">
        <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="profil" style="color: #ffff;">Profil</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="ptk" style="color: #ffff;">PTK</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="sapras" style="color: #ffff;">Sarpras</a>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link  dropdown-toggle" href="" data-bs-toggle="dropdown" style="color: #ffff;"> Rombel </a>
            <ul class="dropdown-menu">
                <li><a class="dropdown-item" href="rombel"> Data Rombel</a></li>
                <li><a class="dropdown-item" href="siswa"> Data Siswa</a></li>
                <li><a class="dropdown-item" href="siswa-prestasi"> Data Siswa Berprestasi</a></li>
            </ul>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link  dropdown-toggle" href="" data-bs-toggle="dropdown" style="color: #ffff;"> Mapel </a>
            <ul class="dropdown-menu">
                <li><a class="dropdown-item" href="mapel"> Mapel Umum</a></li>
                <li><a class="dropdown-item" href="keahlian"> Kompetensi Keahlian</a></li>
            </ul>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link  dropdown-toggle" href="" data-bs-toggle="dropdown" style="color: #ffff;"> Guru </a>
            <ul class="dropdown-menu">
                <li><a class="dropdown-item" href="guru"> Data Guru</a></li>
                <li><a class="dropdown-item" href="guru-prestasi"> Data Guru Berprestasi</a></li>
            </ul>
        </li>

    </ul>
    @if(auth()->user()->can('sekolah'))
    <button type="button" class="fa fa-key btn btn-primary mr-5" data-bs-toggle="modal"
        data-bs-target="#exampleModal"></button>

    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ganti Password</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="/user/update" method="POST">
                        @csrf
                        @method('PATCH')
                        <div class="row">
                            <div class="col-25">
                                <label for="old_password">Password Lama</label>
                            </div>
                            <div class="col-75">
                                <input type="password" name="old_password">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-25">
                                <label for="new_password">Password Baru</label>
                            </div>
                            <div class="col-75">
                                <input type="password" name="new_password">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @endif
    <form method="post" action="/logout">
        @csrf
        <button type="submit" class="btn btn-warning btn-sm">Logout</button>
    </form>
</div>
