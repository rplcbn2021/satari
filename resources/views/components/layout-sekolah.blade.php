<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
	<title>SATARI</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('./css/style.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('./css/styles-index.css') }}"> 
    <link rel="stylesheet" href="{{ asset('./bootstrap/css/bootstrap.min.css') }}" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
	<script src="https://kit.fontawesome.com/206142bfe3.js" crossorigin="anonymous"></script>
	<script src="{{ asset('./bootstrap/js/bootstrap.min.js') }}"></script>
	<style type="text/css">
	.header{
		overflow: hidden;
		padding: 10px 20px;
		margin-bottom: 20px;
	}
	.header h2 {
		display: inline-block;
		padding: 0;
	}
	.header .input-href {
		display: inline-block;
		float: right;
		margin: 0 5px;
	}
	@media screen and (max-width:680px){

	#maincontent{
		width: auto;
		float: none;
	}
	#sidebar{
		width: auto;
		float: none;
	}
	}
	@media (min-width: 991.98px) {
		main {
			padding-left: 7px;
			padding-right: 7px;
			padding-top: 7px;
		}
		}
	</style>
</head>
<body>

	<nav class="navbar navbar-expand-lg navbar-light  navbar-style" style="background-color: #19B16A;">
			<div class="container-fluid">
			<a class="navbar-brand" href="#">
			<img src="{{ asset('images/jabar.png') }}" alt="" width="30" height="35">
			</a>
				<a class="navbar-brand" href="#" style="color: #ffff;">SATARI</a>
				<button class="navbar-toggler bg-light" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
				</button>
				<x-layout-navbar-sekolah />
				</div>
			</div>
		</nav>

{{ $slot }}

</body>
</html>
