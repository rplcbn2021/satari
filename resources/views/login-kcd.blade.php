<x-layout-index>
	<style type="text/css">
		.col-25{
			padding: 15px;
		}
		.container{
			width: 400px;
			position: absolute;
			left: 50%;
			top: 50%;
			transform: translate(-50%, -50%);
		}
		h1{
			font-size: 24px;
			margin-bottom: 20px;
		}
		.container input[type="submit"], .back{
		padding: 15px 30px;
		float: right;
	}
	hr{
		margin: 40px 0;
	}
	.invalid{
		cursor: pointer;
		background: rgba(255, 25, 60, 0.4);
		color: black;
		font-weight: bold;
		padding: 5px;
		border-radius: 10px;
		margin-top: 5px;
		text-transform: uppercase;
		font-size: 14px;
		display: none;
	}
	</style>
</head>
<body style="background: url({{ asset('./images/kcd.jpg') }}); height: 720px; width: 100%; background-size:100% 720px ; display: block;">
	<div class="container">
		<form action="kcd/sessions" method="post">
			@csrf
			<h1 align="center">Login</h1>
			<div class="row">
				<div class="col-25">
					<label for="username">Username</label>
				</div>
				<div class="col-75">
					<input type="text" name="username" autocomplete="off" class="login"  class="login" id="username" required="">
					<div class="invalid"></div>
				</div>
			</div>
			<div class="row">
				<div class="col-25">
					<label for="password">Password</label>
				</div>
				<div class="col-75">
					<input type="password" name="password" autocomplete="off" class="login"  class="login" id="pass" required="">
					<div class="invalid"></div>
				</div>
			</div>
			<div align="center" style="margin-top: 15px;">
				<a class="btn btn-warning" href="index.php" >Back</a>
				<button class="btn btn-success" type="submit" name="submit-kcd" >Login</button>
			</div>
		</form>
	</div>
</body>
</html>
</x-layout-index>