<x-layout-index>
<style type="text/css">
	
	.container input[type="submit"], .back{
		padding: 15px 30px;
		float: right;
	}
	.invalid{
		/* background: rgba(255, 25, 60, 0.4); */
		color: red;
		padding: 5px;
		border-radius: 10px;
		margin-top: 5px;
		text-transform: uppercase;
		font-size: 14px;
	}
	</style>
</head>
<body>
<header class="header">
	<nav class="navbar sticky-top navbar-light bg-light">
	<div class="container-fluid">
	  <a class="navbar-brand">
      <img src="{{ asset('./images/jabar.png') }}" alt="" width="80" height="100" class="d-inline-block align-text-top">
  	  </a>
		<div class="logo-text">
				<h1 style="color: #19B16A;">APLIKASI SATUAN DATA MANDIRI</h1><br>
				<span style="color: #FFA500">CABANG DINAS PENDIDIKAN WILAYAH 1</span><br>
				<span style="color: #FFA500">DINAS PENDIDIKAN PROVINSI JAWA BARAT
					@error('sekolah_id')
						{{ $message }}
					@enderror
					@error('username')
						{{ $message }}
					@enderror
					@error('password')
						{{ $message }}
					@enderror
				</span>
		</div>	
		<a class="btn btn-success btn-sm" href="login-kcd">KCD</a>			
	</div>
	</nav>
</header>
<div class="container-index">
	@include('map.map')
	{{-- <div class="container" style="width: 840px; margin-top: 70px; margin: auto;">
		
			@include('map.bogor')
			<div class="row">
				<div class="col-25">
					<label for="kecamatan">Kecamatan</label>
				</div>
				<div class="col-75">
					<select name="kecamatan" id="kecamatan" required>
						<option value="" hidden>Pilih Kecamatan</option>
							@foreach($kecamatan as $result)

								<option value="{{ $result->id }}"><?php echo $result['nama_kecamatan']?></option>
							
							@endforeach
					</select>
				</div>
			</div>
			<div class="row">
				<div class="col-25">
					<label for="sekolah">Sekolah</label>
				</div>
				<div class="col-75">
					<select name="sekolah" id="sekolah" disabled required></select>
				</div>
			</div>
		<div align="center" style="margin-top:10px;">
		<button id="btn-login" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#school" disabled>Login</button>
		</div>
	</div> --}}
</div> 


{{-- <!-- Modal School--> --}}

@include('form.login-sekolah')

	<script>
		$(document).ready(function(){

			$('svg g#Kecamatan a').click(function(){
				var clicked = $(this).prop('id');
				$('select#kecamatan').val(clicked).change();

			});
			// $('.leaflet-interactive').click(function(){
			// 	$('#school').modal('show');
			// })
			
			var isDragging = false;
			var startingPos = [];
			var dataSekolah = {!! json_encode($sekolah) !!};
			var dataKecamatan = {!! json_encode($kecamatan) !!};
			$(".leaflet-interactive")
					.mousedown(function (evt) {
							isDragging = false;
							startingPos = [evt.pageX, evt.pageY];
					})
					.mousemove(function (evt) {
							if (!(evt.pageX === startingPos[0] && evt.pageY === startingPos[1])) {
									isDragging = true;
							}
					})
					.mouseup(function () {
							if (isDragging) {
									console.log("Drag");
							} else {
									console.log("Click");
									$('#school').modal('show');
									console.log($(this).index()+1);
									var id = $(this).index()+1;
									var kecamatan = dataKecamatan.filter(kecamatan => kecamatan.id == id);
									$('#kecamatan').val(kecamatan[0].nama_kecamatan);
									$('#btn-login').prop('disabled', true);
									if (id > 0) {
										$('#sekolah').find('option').remove().end();
										var sekolah = dataSekolah.filter(sekolah => sekolah.kecamatan_id == id);
										$('#sekolah').append(`<option value="" hidden>Pilih Sekolah</option>`);

										sekolah.forEach(function(result){
											$('#sekolah').append(`<option value="`+result.id+`">`+result.nama+`</option>`);
										});
									}
							}
							isDragging = false;
							startingPos = [];
					});

			
			$('select#sekolah').change(function(){
					console.log('changed');
					$('#btn-login').prop('disabled', false);
					console.log($(this).children("option:selected").val());
			});
			
		});
	</script>


{{-- <!-- Modal KCd-->
<div class="modal fade" id="kcd" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Login KCD</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
	  <div class="container">
		<form action="" method="post">
			<div class="row">
				<div class="col-25">
					<label for="username">Username</label>
				</div>
				<div class="col-75">
					<input type="text" name="username" autocomplete="off" class="login"  class="login" id="username" required="">
					<div class="invalid"></div>
				</div>
			</div>
			<div class="row">
				<div class="col-25">
					<label for="password">Password</label>
				</div>
				<div class="col-75">
					<input type="password" name="password" autocomplete="off" class="login"  class="login" id="pass" required="">
					<div class="invalid"></div>
				</div>
			</div>
			<div class="modal-footer" style="margin-top: 15px;">
			<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
			<button type="submit" class="btn btn-primary" name="submit-kcd">Login</button>
			</div>
		</form>
		</div>
		</form>
		</div>
      </div>
    </div>
  </div>
</div> --}}
	{{-- footer --}}
	<div class="footer">
		<div class="row">
			<span style="color : white">RPL CIBIONE &copy; 2021</span>
		</div>
	</div>
</x-layout-index>