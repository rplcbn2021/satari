<?php

namespace Database\Seeders;

use App\Models\KategoriSekolah;
use Illuminate\Database\Seeder;

class KategoriSekolahSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        KategoriSekolah::insert([
            ['kategori_sekolah' => 'Swasta'],
            ['kategori_sekolah' => 'Negeri']
        ]);
    }
}
