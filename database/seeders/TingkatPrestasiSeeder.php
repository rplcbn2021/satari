<?php

namespace Database\Seeders;

use App\Models\TingkatPrestasi;
use Illuminate\Database\Seeder;

class TingkatPrestasiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TingkatPrestasi::insert([
            [
                'nama_tingkat' => 'Kecamatan'
            ], [
                'nama_tingkat' => 'Kabupaten/Kota',
            ], [
                'nama_tingkat' => 'Provinsi',
            ], [
                'nama_tingkat' => 'Nasional',
            ], [
                'nama_tingkat' => 'Internasional',
            ]
        ]);
    }
}
