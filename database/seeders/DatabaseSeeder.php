<?php

namespace Database\Seeders;

use App\Models\Jabatan;
use App\Models\KategoriSekolah;
use App\Models\Pendidikan;
use App\Models\StatusPegawai;
use App\Models\TingkatPrestasi;
use App\Models\User;
use Database\Factories\UserFactory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            MapelSeeder::class,
            JabatanSeeder::class,
            JurusanSeeder::class,
            PendidikanSeeder::class,
            StatusPegawaiSeeder::class,
            TingkatPrestasiSeeder::class,
            JenjangSeeder::class,
            KategoriSekolahSeeder::class
        ]);

        User::factory(1)->create();
    }
}
