<?php

namespace Database\Seeders;

use App\Models\HakAkses;
use Illuminate\Database\Seeder;

class HakAksesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        HakAkses::insert([
            [
                'hak' => 'operator kcd'
            ],
            [
                'hak' => 'operator sekolah'
            ],
            [
                'hak' => 'kepsek'
            ]
        ]);
    }
}
