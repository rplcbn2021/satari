<?php

namespace Database\Seeders;

use App\Models\Sekolah;
use Illuminate\Database\Seeder;

class SekolahSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Sekolah::insert([
            [
                'kecamatan_id' => '1',
                'jenjang_id' => '1',
                'npsn' => '23242349',
                'nss' => '37498178',
                'nama' => 'SMKN 1 Cibinong',
                'alamat' => 'Karadenan'
            ],
            [
                'kecamatan_id' => '1',
                'jenjang_id' => '1',
                'npsn' => '23249',
                'nss' => '37178',
                'nama' => 'SMKN 2 Cibinong',
                'alamat' => 'idk'
            ],
        ]);
        Sekolah::create([
            'kecamatan_id' => '2',
            'jenjang_id' => '1',
            'nama' => 'SMAN 2 Citeureup',
            'alamat' => 'Citeureup'
        ]);
    }
}
