<?php

namespace Database\Seeders;

use App\Models\Pendidikan;
use Illuminate\Database\Seeder;

class PendidikanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Pendidikan::insert([
            [
                'pendidikan' => 'Sarjana 1',
            ], [
                'pendidikan' => 'Sarjana 2',
            ], [
                'pendidikan' => 'Sarjana 3',
            ]
        ]);
    }
}
