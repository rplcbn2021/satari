<?php

namespace Database\Seeders;

use App\Models\Jenjang;
use Illuminate\Database\Seeder;

class JenjangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Jenjang::insert([
            ['nama_jenjang' => 'Sekolah Menengah Atas',],
            ['nama_jenjang' => 'Sekolah Menengah Kejuruan',],
            ['nama_jenjang' => 'Sekolah Luar Biasa']
        ]);
    }
}
