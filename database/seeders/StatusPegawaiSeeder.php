<?php

namespace Database\Seeders;

use App\Models\StatusPegawai;
use Illuminate\Database\Seeder;

class StatusPegawaiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        StatusPegawai::insert([
            [
                'status' => 'PNS'
            ], [
                'status' => 'Non PNS'
            ]
        ]);
    }
}
