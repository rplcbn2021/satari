<?php

namespace Database\Seeders;

use App\Models\Mapel;
use Illuminate\Database\Seeder;

class MapelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Mapel::insert([
            [
                'nama_mapel' => 'Pendidikan Agama dan Budi Pekerti',
                'blok' => '1'
            ], [
                'nama_mapel' => 'Pendidikan Pancasila dan Kewarganegaraan',
                'blok' => '1'
            ], [
                'nama_mapel' => 'Bahasa Indonesia',
                'blok' => '1'
            ], [
                'nama_mapel' => 'Matematika',
                'blok' => '1'
            ], [
                'nama_mapel' => 'Sejarah Indonesia',
                'blok' => '1'
            ], [
                'nama_mapel' => 'Bahasa Inggris',
                'blok' => '1'
            ], [
                'nama_mapel' => 'Seni Budaya',
                'blok' => '1'
            ], [
                'nama_mapel' => 'Pendidikan Jasmani Olahraga dan Kesehatan',
                'blok' => '1'
            ], [
                'nama_mapel' => 'Fisika',
                'blok' => '1'
            ], [
                'nama_mapel' => 'Kimia',
                'blok' => '1'
            ]
        ]);

        Mapel::insert([
            [
                'nama_mapel' => 'Simulasi dan Komunikasi Digital',
                'blok' => '2',
                'jurusan_id' => 3
            ], [
                'nama_mapel' => 'Sistem Komputer',
                'blok' => '2',
                'jurusan_id' => 3
            ], [
                'nama_mapel' => 'Komputer dan Jaringan Dasar',
                'blok' => '2',
                'jurusan_id' => 3
            ], [
                'nama_mapel' => 'Pemograman Dasar',
                'blok' => '2',
                'jurusan_id' => 3
            ]
        ]);
    }
}
