<?php

namespace Database\Seeders;

use App\Models\Jabatan;
use Illuminate\Database\Seeder;

class JabatanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Jabatan::insert([
            [
                'jabatan' => 'Ketua Komite',

            ], [
                'jabatan' => 'Kepala Sekolah',

            ], [
                'jabatan' => 'Kepala Tata Usaha',

            ], [
                'jabatan' => 'Wakil SARPRASHUM',

            ], [
                'jabatan' => 'Wakil Kurikulum',

            ], [
                'jabatan' => 'Wakil Kesiswaan',

            ], [
                'jabatan' => 'Pembina OSIS',

            ], [
                'jabatan' => 'Koordinator Ekstrakulikuler',

            ], [
                'jabatan' => 'Kepala LAB',

            ], [
                'jabatan' => 'Kepala Perpustakaan',

            ], [
                'jabatan' => 'Guru Bimbingan Konseling',

            ], [
                'jabatan' => 'Guru Mata Pelajaran',

            ], [
                'jabatan' => 'Wali Kelas',

            ], [
                'jabatan' => 'Guru Piket',

            ]
        ]);
    }
}
