<?php

use App\Http\Controllers\CreateKcdController;
use App\Http\Controllers\CrudGuruController;
use App\Http\Controllers\CrudGuruPrestasiController;
use App\Http\Controllers\CrudKeahlianController;
use App\Http\Controllers\CrudMapelController;
use App\Http\Controllers\CrudPtkController;
use App\Http\Controllers\CrudRombelController;
use App\Http\Controllers\CrudSaprasController;
use App\Http\Controllers\CrudSiswaPrestasiController;
use App\Http\Controllers\DeleteSekolahController;
use App\Http\Controllers\ImportSekolahController;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\KcdController;
use App\Http\Controllers\KcdExportController;
use App\Http\Controllers\KcdGuruController;
use App\Http\Controllers\KcdGuruPrestasiController;
use App\Http\Controllers\KcdImportController;
use App\Http\Controllers\KcdKeahlianController;
use App\Http\Controllers\KcdMapelController;
use App\Http\Controllers\KcdProfilController;
use App\Http\Controllers\KcdPtkController;
use App\Http\Controllers\KcdRombelController;
use App\Http\Controllers\KcdSaprasController;
use App\Http\Controllers\KcdSekolahController;
use App\Http\Controllers\KcdSiswaController;
use App\Http\Controllers\KcdSuratController;
use App\Http\Controllers\SekolahGuruController;
use App\Http\Controllers\SekolahMapelController;
use App\Http\Controllers\SekolahProfilController;
use App\Http\Controllers\SekolahPtkController;
use App\Http\Controllers\SekolahRombelController;
use App\Http\Controllers\SekolahSaprasController;
use App\Http\Controllers\SekolahSiswaPController;
use App\Http\Controllers\SekolahUserController;
use App\Http\Controllers\UpdateSekolahController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//index
Route::get('/', [IndexController::class, 'index']);

//index->login sekolah

Route::get('/login-sekolah', [IndexController::class, 'loginSekolah']);
Route::get('/login-kcd', [IndexController::class, 'loginKcd']);
Route::post('sekolah/sessions', [IndexController::class, 'storeSekolah']);
Route::post('kcd/sessions', [IndexController::class, 'storeKcd']);
Route::post('logout', [IndexController::class, 'destroy']);

//KCD


Route::group(['middleware' => ['kcd']], function () {
  Route::get('/kcd/kecamatan', [KcdController::class, 'kecamatan']);
  Route::post('/kcd/kecamatan/import', [KcdImportController::class, 'kecamatan']);
  Route::get('/kcd/kecamatan/user-export', [KcdExportController::class, 'exportUser']);
  Route::get('/kcd/kecamatan/{kecamatan}/sekolah', [KcdController::class, 'sekolah']);
  Route::get('/kcd/sekolah/{sekolah}/hapus', [KcdController::class, 'deleteSekolah']);
  Route::get('/kcd/sekolah/{sekolah}/surat-rekomendasi', [KcdSuratController::class, 'suratPDF']);
  Route::get('/kcd/sekolah/{sekolah}/profil', [KcdProfilController::class, 'profil']);
  Route::get('/kcd/sekolah/{sekolah}/rombel', [KcdRombelController::class, 'rombel']);
  Route::get('/kcd/sekolah/{sekolah}/siswa', [KcdSiswaController::class, 'siswa']);
  Route::get('/kcd/sekolah/{sekolah}/siswa-prestasi', [KcdSiswaController::class, 'siswaPrestasi']);
  Route::get('/kcd/sekolah/{sekolah}/keahlian', [KcdKeahlianController::class, 'keahlian']);
  Route::get('/kcd/sekolah/{sekolah}/mapel', [KcdMapelController::class, 'mapel']);
  Route::get('/kcd/sekolah/{sekolah}/guru', [KcdGuruController::class, 'guru']);
  Route::get('/kcd/sekolah/{sekolah}/guru-prestasi', [KcdGuruPrestasiController::class, 'guruPrestasi']);
  Route::get('/kcd/sekolah/{sekolah}/ptk', [KcdPtkController::class, 'ptk']);
  Route::get('/kcd/sekolah/{sekolah}/sapras', [KcdSaprasController::class, 'sapras']);
  Route::get('/kcd/sekolah/{sekolah}/profil', [KcdSekolahController::class, 'profil']);
  // Route::get('/kcd/kecamatan/{kecamatan}/sekolah/input', [KcdController::class, 'inputSekolah']);
  Route::post('/kcd/kecamatan/sekolah/import', [KcdImportController::class, 'sekolah']);
  Route::post('/kcd/kecamatan/sekolah/create', [CreateKcdController::class, 'createSekolah']);
});
//sekolah
// profil sekolah

Route::group(['middleware' => ['sekolah']], function () {

  Route::get('/sekolah', [SekolahProfilController::class, 'profil']);
  Route::get('/sekolah/profil', [SekolahProfilController::class, 'profil']);
  Route::get('/sekolah/ptk/', [SekolahPtkController::class, 'ptk']);
  Route::get('/sekolah/guru', [SekolahGuruController::class, 'guru']);
  Route::get('/sekolah/guru-prestasi', [SekolahGuruController::class, 'guruPrestasi']);
  Route::get('/sekolah/sapras', [SekolahSaprasController::class, 'sapras']);
  Route::get('/sekolah/rombel', [SekolahRombelController::class, 'rombel']);
  Route::get('/sekolah/rombel/export', [SekolahRombelController::class, 'exportRombel']);
  Route::get('/sekolah/siswa', [SekolahRombelController::class, 'siswa']);
  Route::get('/sekolah/siswa-prestasi', [SekolahSiswaPController::class, 'siswaPrestasi']);
  Route::get('/sekolah/mapel', [SekolahMapelController::class, 'mapel']);
  Route::get('/sekolah/keahlian', [SekolahMapelController::class, 'keahlian']);
  Route::get('/sekolah/user', [SekolahUserController::class, 'user']);
  Route::patch('/user/update', [SekolahUserController::class, 'updateUser']);
});

Route::group(['middleware' => ['operator_sekolah']], function () {

  Route::get('/sekolah/profil/edit', [SekolahProfilController::class, 'editProfil']);
  Route::patch('/sekolah/profil/update', [UpdateSekolahController::class, 'updateProfil']);

  // Route::get('/sekolah/ptk/input', [SekolahPtkController::class, 'inputPtk']);
  Route::get('/sekolah/ptk/{ptk}/hapus', [CrudPtkController::class, 'deletePtk']);
  Route::post('/sekolah/ptk/create', [CrudPtkController::class, 'createPtk']);
  Route::post('/sekolah/ptk/import', [ImportSekolahController::class, 'importptk']);
  Route::patch('/sekolah/ptk/update', [CrudPtkController::class, 'updatePtk']);

  Route::get('/sekolah/guru/{guru}/hapus', [CrudGuruController::class, 'deleteGuru']);
  Route::post('/sekolah/guru/import', [ImportSekolahController::class, 'importGuru']);
  Route::post('/sekolah/guru/create', [CrudGuruController::class, 'createGuru']);
  Route::patch('/sekolah/guru/update', [CrudGuruController::class, 'updateGuru']);

  Route::get('/sekolah/sapras/{sapras}/hapus', [CrudSaprasController::class, 'deleteSapras']);
  Route::post('/sekolah/sapras/create', [CrudSaprasController::class, 'createSapras']);
  Route::post('/sekolah/sapras/import', [ImportSekolahController::class, 'importSapras']);
  Route::patch('/sekolah/sapras/update', [CrudSaprasController::class, 'updateSapras']);

  Route::get('/sekolah/rombel/{rombel}/hapus', [CrudRombelController::class, 'deleteRombel']);
  Route::post('/sekolah/rombel/create', [CrudRombelController::class, 'createRombel']);
  Route::post('/sekolah/rombel/import', [ImportSekolahController::class, 'importRombel']);
  Route::patch('/sekolah/rombel/update', [CrudRombelController::class, 'updateRombel']);

  Route::get('/sekolah/guru-prestasi/{guru_prestasi}/hapus', [CrudGuruPrestasiController::class, 'deleteGuruPrestasi']);
  Route::post('/sekolah/guru-prestasi/create', [CrudGuruPrestasiController::class, 'createGuruPrestasi']);
  Route::patch('/sekolah/guru-prestasi/update', [CrudGuruPrestasiController::class, 'updateGuruPrestasi']);

  Route::get('/sekolah/siswa/edit', [SekolahRombelController::class, 'editSiswa']);
  Route::patch('/sekolah/siswa/update', [CrudRombelController::class, 'updateSiswa']);

  Route::get('/sekolah/siswa-prestasi/{siswa}/hapus', [CrudSiswaPrestasiController::class, 'deleteSiswaPrestasi']);
  Route::post('/sekolah/siswa-prestasi/create', [CrudSiswaPrestasiController::class, 'createSiswaPrestasi']);
  Route::patch('/sekolah/siswa-prestasi/update', [CrudSiswaPrestasiController::class, 'updateSiswaPrestasi']);

  Route::get('/sekolah/mapel/{jam}/hapus', [CrudMapelController::class, 'deleteMapel']);
  Route::post('/sekolah/mapel/create', [CrudMapelController::class, 'createMapel']);
  Route::post('/sekolah/mapel/import', [ImportSekolahController::class, 'importMapel']);
  Route::patch('/sekolah/mapel/update', [CrudMapelController::class, 'updateMapel']);

  Route::get('/sekolah/keahlian/{jam}/hapus', [CrudKeahlianController::class, 'deleteKeahlian']);
  Route::post('/sekolah/keahlian/create', [CrudKeahlianController::class, 'createKeahlian']);
  Route::post('/sekolah/keahlian/import', [ImportSekolahController::class, 'importKeahlian']);
  Route::patch('/sekolah/keahlian/update', [CrudKeahlianController::class, 'updateKeahlian']);
});
