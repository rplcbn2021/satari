<?php

namespace App\Providers;

use App\Models\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //

        Gate::define('operator_sekolah', function (User $user) {
            return $user->hak_akses_id == 1;
        });

        Gate::define('sekolah', function (User $user) {
            return $user->hak_akses_id < 3;
        });

        Gate::define('kcd', function (User $user) {
            return $user->hak_akses_id == 3;
        });
    }
}
