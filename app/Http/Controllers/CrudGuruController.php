<?php

namespace App\Http\Controllers;

use App\Models\Guru;
use App\Models\Jam;
use App\Models\Mapel;
use App\Models\Sekolah;
use App\Models\TingkatKebutuhan;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class CrudGuruController extends Controller
{
    public function createGuru()
    {

        $sekolah = Sekolah::find(auth()->user()->sekolah_id);


        $attributes = request()->validate([
            'nip' => 'required',
            'nama' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'alamat' => 'required',
            'telepon' => 'nullable',
            'email' => 'nullable',
            'status_pegawai_id' => ['required', Rule::exists('status_pegawai', 'id')],
            'pendidikan_id' => ['required', Rule::exists('pendidikan', 'id')],
            'mapel_id' => 'required'

        ]);

        // if (!Mapel::where('nama_mapel', $attributes['mapel_id'])->first()) {

        //     Mapel::create([
        //         'nama_mapel' => $attributes['mapel_id']
        //     ]);
        // }

        $attributes['sekolah_id'] = $sekolah->id;

        $guru = Guru::create($attributes);

        $kebutuhan = [];
        $jam = TingkatKebutuhan::where('jam_id', Jam::where('mapel_id', $guru->mapel_id)->first()->id)->first();

        $tb = $sekolah->guru->where('mapel_id', $attributes['mapel_id'])->count() - $jam->quota;
        $kurang = 0;
        $lebih = 0;
        $tb > 0 ? $lebih = $tb  : $kurang = abs($tb);
        $kebutuhan['lebih'] = $lebih;
        $kebutuhan['kurang'] = $kurang;

        TingkatKebutuhan::find($jam->id)->update($kebutuhan);

        return redirect("/sekolah/guru");
    }

    public function updateGuru()
    {

        $attributes = request()->validate([
            'id' => ['required', Rule::exists('guru', 'id')],
            'nip' => 'required',
            'nama' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'alamat' => 'required',
            'telepon' => '',
            'email' => '',
            'status_pegawai_id' => ['required', Rule::exists('status_pegawai', 'id')],
            'pendidikan_id' => ['required', Rule::exists('pendidikan', 'id')],
            'mapel_id' => ['required', Rule::exists('mapel', 'id')],

        ]);

        $id = $attributes['id'];
        unset($attributes['id']);

        Guru::find($id)->update($attributes);

        $kebutuhan = [];
        $jam = TingkatKebutuhan::where('jam_id', Jam::where('mapel_id', $attributes['mapel_id'])->first()->id)->first();

        $tb = Guru::where('mapel_id', $attributes['mapel_id'])->count() - $jam->quota;
        $kurang = 0;
        $lebih = 0;
        $tb > 0 ? $lebih = $tb  : $kurang = abs($tb);
        $kebutuhan['lebih'] = $lebih;
        $kebutuhan['kurang'] = $kurang;

        TingkatKebutuhan::find($jam->id)->update($kebutuhan);

        return redirect("/sekolah/guru");
    }

    public function deleteGuru(Guru $guru)
    {

        $guru->delete();

        $kebutuhan = [];
        $jam = TingkatKebutuhan::where('jam_id', Jam::where('mapel_id', $guru->mapel_id)->first()->id)->first();

        $tb = Guru::where('mapel_id', $guru->mapel_id)->count() - $jam->quota;
        $kurang = 0;
        $lebih = 0;
        $tb > 0 ? $lebih = $tb  : $kurang = abs($tb);
        $kebutuhan['lebih'] = $lebih;
        $kebutuhan['kurang'] = $kurang;

        TingkatKebutuhan::find($jam->id)->update($kebutuhan);

        return redirect("/sekolah/guru");
    }
}
