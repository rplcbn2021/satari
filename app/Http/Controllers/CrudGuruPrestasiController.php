<?php

namespace App\Http\Controllers;

use App\Models\Guru;
use App\Models\GuruPrestasi;
use App\Models\Sekolah;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class CrudGuruPrestasiController extends Controller
{
    public function createGuruPrestasi()
    {

        $sekolah = Sekolah::find(auth()->user()->sekolah_id);

        $attributes = request()->validate([
            'nip' => ['required', Rule::exists('guru', 'nip')],
            'tingkat_id' => ['required', Rule::exists('tingkat_prestasi', 'id')],
            'prestasi_guru' => 'required'
        ]);
        $id_guru = Guru::where('nip', $attributes['nip'])->without(['status_pegawai', 'mapel', 'pendidikan'])->first();
        $attributes['guru_id'] = $id_guru->id;
        unset($attributes['nip']);
        $attributes['sekolah_id'] = $sekolah->id;

        GuruPrestasi::create($attributes);
        return redirect()->back();
    }

    public function updateGuruPrestasi()
    {
        $sekolah = Sekolah::find(auth()->user()->sekolah_id);

        $attributes = request()->validate([
            'id' => ['required', Rule::exists('guru_prestasi', 'id')],
            'tingkat_id' => ['required', Rule::exists('tingkat_prestasi', 'id')],
            'prestasi_guru' => 'required'
        ]);

        $attributes['sekolah_id'] = $sekolah->id;
        $id = $attributes['id'];
        unset($attributes['id']);

        GuruPrestasi::find($id)->update($attributes);
        return redirect()->back();
    }

    public function deleteGuruPrestasi(GuruPrestasi $guru_prestasi)
    {
        $guru_prestasi->delete();

        return redirect()->back();
    }
}
