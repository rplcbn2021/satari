<?php

namespace App\Http\Controllers;

use App\Models\Guru;
use App\Models\Jam;
use App\Models\Ptk;
use App\Models\Rombel;
use App\Models\Sapras;
use App\Models\Sekolah;
use App\Models\SiswaPrestasi;
use App\Models\TingkatKebutuhan;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class UpdateSekolahController extends Controller
{
    public function updateProfil()
    {

        $sekolah = Sekolah::find(auth()->user()->sekolah_id);

        $attributes = request()->validate([
            'npsn' => 'required',
            'nss' => 'nullable',
            'nama' => 'required',
            'alamat' => 'nullable',
            'telepon' => 'nullable',
            'fax' => 'nullable',
            'email' => 'nullable'
        ]);

        $attributes['id'] = $sekolah->id;

        $sekolah->update($attributes);

        return redirect()->back();
    }
}
