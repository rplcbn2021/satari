<?php

namespace App\Http\Controllers;

use App\Models\Jam;
use App\Models\Sapras;
use App\Models\Sekolah;
use PDF;
use Illuminate\Http\Request;

class KcdSuratController extends Controller
{
    public function suratPDF(Sekolah $sekolah)
    {
        $data = [
            'sekolah' => $sekolah,
            'jam' => $sekolah->jam->load(['mapel', 'tingkat_kebutuhan']),
            'guru' => $sekolah->guru,
            'tingkat_kebutuhan' => $sekolah->tingkat_kebutuhan,
            'guru_prestasi' => $sekolah->guru_prestasi->load(['guru']),
            'siswa_prestasi' => $sekolah->siswa_prestasi,
            'sapras' => $sekolah->sapras,
            'rombel' => $sekolah->rombel->load(['siswa'])
        ];

        // return view('pdf.surat-rekomendasi_pdf', $data);

        $pdf = PDF::loadView('pdf.surat-rekomendasi_pdf', $data);

        return $pdf->stream('surat-rekomendasi.pdf');
    }
}
