<?php

namespace App\Http\Controllers;

use App\Models\Sekolah;
use App\Models\Siswa;
use App\Models\SiswaPrestasi;
use App\Models\TingkatPrestasi;
use Illuminate\Http\Request;

class SekolahSiswaPController extends Controller
{

    public function siswaPrestasi()
    {

        $sekolah = Sekolah::find(auth()->user()->sekolah_id);

        return view('sekolah.siswa-prestasi', [
            'page' => 'input',
            'data' => $sekolah->siswa_prestasi->load(['tingkat_prestasi']),
            'tingkat' => TingkatPrestasi::all()

        ]);
    }
}
