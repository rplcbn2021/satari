<?php

namespace App\Http\Controllers;

use App\Models\Sekolah;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class SekolahProfilController extends Controller
{
    public function profil()
    {
        $sekolah = Sekolah::find(auth()->user()->sekolah_id);

        return view('sekolah.profil', [
            'page' => 'profil',
            'data' => $sekolah->load(['rombel', 'siswa', 'guru', 'guru_prestasi', 'ptk', 'sapras'])
        ]);
    }

    public function editProfil()
    {

        $sekolah = Sekolah::find(auth()->user()->sekolah_id);

        return view('operator.edit-profil', [
            'page' => 'edit',
            'data' => $sekolah
        ]);
    }
}
