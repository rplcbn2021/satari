<?php

namespace App\Http\Controllers;

use App\Models\Sekolah;
use Illuminate\Http\Request;

class KcdSekolahController extends Controller
{
    public function profil(Sekolah $sekolah)
    {
        return view('sekolah.profil', [
            'page' => 'profil',
            'data' => $sekolah
        ]);
    }
}
