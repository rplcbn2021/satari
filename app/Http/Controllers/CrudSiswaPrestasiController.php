<?php

namespace App\Http\Controllers;

use App\Models\Sekolah;
use App\Models\Siswa;
use App\Models\SiswaPrestasi;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class CrudSiswaPrestasiController extends Controller
{
    public function createSiswaPrestasi()
    {

        $sekolah = auth()->user()->sekolah_id;

        $attributes = request()->validate([
            'nis' => 'required',
            'nama' => 'required',
            'kelas' => 'required',
            'prestasi_siswa' => 'required',
            'event' => 'required',
            'tanggal_event' => 'required',
            'tingkat_id' => 'required'
        ]);

        $attributes['sekolah_id'] = $sekolah;

        SiswaPrestasi::create($attributes);

        return redirect()->back();
    }

    public function updateSiswaPrestasi()
    {

        $sekolah = Sekolah::find(auth()->user()->sekolah_id);

        $attributes = request()->validate([
            'id' => ['required', Rule::exists('siswa_prestasi', 'id')->where('sekolah_id', $sekolah->id)],
            'nama' => 'required',
            'kelas' => 'required',
            'prestasi_siswa' => 'required',
            'event' => 'required',
            'tanggal_event' => 'required',
            'tingkat_id' => 'required'
        ]);

        $id = $attributes['id'];
        unset($attributes['id']);

        $attributes['sekolah_id'] = $sekolah->id;

        SiswaPrestasi::find($id)->update($attributes);

        return redirect()->back();
    }

    public function deleteSiswaPrestasi(SiswaPrestasi $siswa)
    {

        $siswa->delete();

        return redirect()->back();
    }
}
