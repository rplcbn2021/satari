<?php

namespace App\Http\Controllers;

use App\Imports\KecamatanImport;
use App\Imports\SekolahImport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class KcdImportController extends Controller
{
    public function sekolah()
    {

        Excel::import(new SekolahImport, request()->file('excel'));

        return redirect()->back();
    }

    public function kecamatan()
    {

        Excel::import(new KecamatanImport, request()->file('excel'));

        return redirect()->back();
    }
}
