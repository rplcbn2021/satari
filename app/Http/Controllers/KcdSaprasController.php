<?php

namespace App\Http\Controllers;

use App\Models\Sekolah;
use Illuminate\Http\Request;

class KcdSaprasController extends Controller
{
    public function sapras(Sekolah $sekolah)
    {
        return view('sekolah.sapras', [
            'page' => 'sapras',
            'data' => $sekolah->sapras
        ]);
    }
}
