<?php

namespace App\Http\Controllers;

use App\Models\Jurusan;
use App\Models\Sekolah;
use App\Models\TingkatPrestasi;
use Illuminate\Http\Request;

class KcdSiswaController extends Controller
{
    public function siswa(Sekolah $sekolah)
    {

        return view('sekolah.siswa', [
            'page' => 'siswa',
            'data' => $sekolah->siswa->load(['rombel']),
            'jurusan' => Jurusan::all(),
        ]);
    }

    public function siswaPrestasi(Sekolah $sekolah)
    {
        return view('sekolah.siswa-prestasi', [
            'page' => 'input',
            'data' => $sekolah->siswa_prestasi->load(['tingkat_prestasi']),
            'tingkat' => TingkatPrestasi::all()
        ]);
    }
}
