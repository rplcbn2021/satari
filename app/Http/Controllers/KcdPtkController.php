<?php

namespace App\Http\Controllers;

use App\Models\Sekolah;
use Illuminate\Http\Request;

class KcdPtkController extends Controller
{
    public function ptk(Sekolah $sekolah)
    {

        return view('sekolah.ptk', [
            'page' => 'ptk',
            'data' => $sekolah->ptk
        ]);
    }
}
