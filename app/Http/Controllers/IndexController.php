<?php

namespace App\Http\Controllers;

use App\Models\Kecamatan;
use App\Models\Sekolah;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class IndexController extends Controller
{

    public function index()
    {
        return view('index', [
            'kecamatan' => Kecamatan::all()->sortBy('nama_kecamatan'),
            'sekolah' => Sekolah::all(['id', 'kecamatan_id', 'nama'])
        ]);
    }

    public function loginSekolah()
    {
        return view('login-sekolah', [
            'sekolah' => Sekolah::all(),
            'kecamatan' => Kecamatan::all()
        ]);
    }

    public function loginKcd()
    {
        return view('login-kcd');
    }

    public function storeSekolah()
    {
        $attributes = request()->validate([
            'username' => 'required',
            'password' => 'required'
        ]);

        if (auth()->attempt($attributes)) {
            return redirect('/sekolah/profil');
        }

        throw ValidationException::withMessages([
            'sekolah_id' => 'id error'
        ]);

        return back();
    }

    public function storeKcd()
    {
        $attributes = request()->validate([
            'username' => 'required',
            'password' => 'required'
        ]);

        $attributes['hak_akses_id'] = 3;

        if (auth()->attempt($attributes)) {
            return redirect('/kcd/kecamatan');
        }

        throw ValidationException::withMessages([]);

        return back();
    }

    public function destroy()
    {
        auth()->logout();

        return redirect('/');
    }
}
