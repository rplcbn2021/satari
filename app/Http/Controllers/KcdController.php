<?php

namespace App\Http\Controllers;

use App\Models\Jenjang;
use App\Models\KategoriSekolah;
use App\Models\Kecamatan;
use App\Models\Sekolah;
use Illuminate\Http\Request;

class KcdController extends Controller
{
    public function kecamatan()
    {

        return view('kcd.kecamatan', [
            'page' => 'kecamatan',
            'kecamatan' => Kecamatan::all()->load(['sekolah']),
            'jenjang' => Jenjang::all(),
            'kategori' => KategoriSekolah::all()
        ]);
    }
    // public function inputSekolah(Kecamatan $kecamatan)
    // {
    //     return view('kcd.input-sekolah', [
    //         'page' => 'input',
    //         'kecamatan' => $kecamatan,
    //         'jenjang' => Jenjang::all(),
    //         'kategori' => KategoriSekolah::all()
    //     ]);
    // }
    public function sekolah(Kecamatan $kecamatan)
    {
        return view('kcd.sekolah', [
            'page' => 'sekolah',
            'jenjang' => Jenjang::all(),
            'kategori' => KategoriSekolah::all(),
            'kecamatan' => $kecamatan,
            'data' => $kecamatan->sekolah->sortBy('nama')
        ]);
    }

    public function deleteSekolah(Sekolah $sekolah)
    {

        foreach ($sekolah->ptk as $ptk) {
            $ptk->delete();
        }

        foreach ($sekolah->guru_prestasi as $guru_prestasi) {
            $guru_prestasi->delete();
        }
        foreach ($sekolah->siswa_prestasi as $siswa_prestasi) {
            $siswa_prestasi->delete();
        }
        foreach ($sekolah->guru as $guru) {
            $guru->delete();
        }
        foreach ($sekolah->sapras as $sapras) {
            $sapras->delete();
        }
        foreach ($sekolah->siswa as $siswa) {
            $siswa->delete();
        }
        foreach ($sekolah->rombel as $rombel) {
            $rombel->delete();
        }
        foreach ($sekolah->tingkat_kebutuhan as $tingkat_kebutuhan) {
            $tingkat_kebutuhan->delete();
        }
        foreach ($sekolah->jam as $jam) {
            $jam->delete();
        }

        foreach ($sekolah->user as $user) {
            if ($user->old_user) {
                $user->old_user->delete();
            }
            $user->delete();
        }

        $sekolah->delete();

        return redirect('/kcd/kecamatan/' . $sekolah->kecamatan_id . '/sekolah');
    }
}
