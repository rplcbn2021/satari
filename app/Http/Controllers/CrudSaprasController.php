<?php

namespace App\Http\Controllers;

use App\Models\Sapras;
use App\Models\Sekolah;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class CrudSaprasController extends Controller
{
    public function createSapras()
    {

        $sekolah = Sekolah::find(auth()->user()->sekolah_id);

        $attributes = request()->validate([
            'ruang' => 'required',
            'nama' => 'required',
            'kondisi_baik' => 'numeric',
            'kondisi_sedang' => 'numeric',
            'kondisi_berat' => 'numeric'
        ]);

        $attributes['sekolah_id'] = $sekolah->id;

        Sapras::create($attributes);

        return redirect("/sekolah/sapras");
    }

    public function updateSapras()
    {

        $sekolah = Sekolah::find(auth()->user()->sekolah_id);

        $attributes = request()->validate([
            'id' => ['required', Rule::exists('sapras', 'id')->where('sekolah_id', $sekolah->id)],
            'ruang' => 'required',
            'nama' => 'required',
            'kondisi_baik' => 'numeric',
            'kondisi_sedang' => 'numeric',
            'kondisi_berat' => 'numeric'
        ]);

        $id = $attributes['id'];
        unset($attributes['id']);

        $attributes['sekolah_id'] = $sekolah->id;

        Sapras::find($id)->update($attributes);

        return redirect()->back();
    }

    public function deleteSapras(Sapras $sapras)
    {
        $sapras->delete();

        return redirect("/sekolah/sapras");
    }
}
