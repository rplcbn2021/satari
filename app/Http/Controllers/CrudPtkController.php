<?php

namespace App\Http\Controllers;

use App\Models\Ptk;
use App\Models\Sekolah;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class CrudPtkController extends Controller
{
    public function createPtk()
    {

        $sekolah = Sekolah::find(auth()->user()->sekolah_id);

        $attributes = request()->validate([
            'nip' => ['required', Rule::unique('ptk', 'nip')],
            'nama' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => ['required', 'date'],
            'alamat' => 'required',
            'telepon' => 'nullable',
            'email' => 'nullable',
            'status_pegawai_id' => ['required', Rule::exists('status_pegawai', 'id')],
            'pendidikan_id' => ['required', Rule::exists('pendidikan', 'id')],
            'jabatan_id' => ['required', Rule::exists('jabatan', 'id')]
        ]);

        $attributes['sekolah_id'] = $sekolah->id;

        Ptk::create($attributes);

        return redirect("/sekolah/ptk");
    }

    public function updatePtk()
    {

        $sekolah = Sekolah::find(auth()->user()->sekolah_id);

        $attributes = request()->validate([
            'id' => ['required', Rule::exists('ptk', 'id')],
            'nip' => 'required',
            'nama' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'alamat' => 'required',
            'telepon' => 'nullable',
            'email' => 'nullable',
            'status_pegawai_id' => ['required', Rule::exists('status_pegawai', 'id')],
            'pendidikan_id' => ['required', Rule::exists('pendidikan', 'id')],
            'jabatan_id' => ['required', Rule::exists('jabatan', 'id')]
        ]);

        $attributes['sekolah_id'] = $sekolah->id;

        $id = $attributes['id'];
        unset($attributes['id']);

        Ptk::find($id)->update($attributes);

        return redirect()->back();
    }

    public function deletePtk(Ptk $ptk)
    {

        $ptk->delete();

        return redirect("/sekolah/ptk");
    }
}
