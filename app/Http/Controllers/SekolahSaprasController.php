<?php

namespace App\Http\Controllers;

use App\Models\Sapras;
use App\Models\Sekolah;
use Illuminate\Http\Request;

class SekolahSaprasController extends Controller
{
    public function sapras()
    {
        $sekolah = Sekolah::find(auth()->user()->sekolah_id);

        return view('sekolah.sapras', [
            'page' => 'sapras',
            'data' => $sekolah->sapras
        ]);
    }
}
