<?php

namespace App\Http\Controllers;

use App\Models\Sekolah;
use Illuminate\Http\Request;

class KcdProfilController extends Controller
{
    public function profil(Sekolah $sekolah)
    {
        return view('sekolah.profil', [
            'page' => 'Profil Sekolah',
            'data' => $sekolah
        ]);
    }
}
