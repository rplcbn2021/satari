<?php

namespace App\Http\Controllers;

use App\Exports\FirstUserExport;
use App\Models\OldUser;
use App\Models\Sekolah;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Facades\Excel;

class CreateKcdController extends Controller
{
    public function createSekolah()
    {

        $attributes = request()->validate([
            'kecamatan_id' => ['required', Rule::exists('kecamatan', 'id')],
            'npsn' => ['required', 'unique:sekolah,npsn'],
            'nss' => ['required', 'unique:sekolah,nss'],
            'nama' => 'required',
            'jenjang_id' => ['required', Rule::exists('jenjang', 'id')],
            'kategori_sekolah_id' => ['required', Rule::exists('kategori_sekolah', 'id')]

        ]);

        $sekolah = Sekolah::create($attributes);

        $password1 = Str::random(8);
        $password2 = Str::random(8);

        $userOperator = User::create([
            'username' => 'operator' . $sekolah->npsn,
            'password' => Hash::make($password1),
            'sekolah_id' => $sekolah->id,
            'hak_akses_id' => 1,
            'remember_token' => Str::random(10)
        ]);

        $userKepsek = User::create([
            'username' => 'kepsek' . $sekolah->npsn,
            'password' => Hash::make($password2),
            'sekolah_id' => $sekolah->id,
            'hak_akses_id' => 2,
            'remember_token' => Str::random(10)
        ]);

        OldUser::create([
            'users_id' => $userOperator->id,
            'username' => $userOperator->username,
            'first_password' => $password1
        ]);

        OldUser::create([
            'users_id' => $userKepsek->id,
            'username' => $userKepsek->username,
            'first_password' => $password2
        ]);

        return redirect('kcd/kecamatan');
    }
}
