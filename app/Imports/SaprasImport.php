<?php

namespace App\Imports;

use App\Models\Sapras;
use App\Models\Sekolah;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class SaprasImport implements ToCollection, WithHeadingRow
{

    public function collection(Collection $rows)
    {
        $sekolah = Sekolah::find(auth()->user()->sekolah_id);

        foreach ($rows as $row) {

            $attributes = [
                'sekolah_id' => $sekolah->id,
                'ruang' => $row['ruang'],
                'nama' => $row['nama_barang'],
                'kondisi_baik' => $row['baik'],
                'kondisi_sedang' => $row['sedang'],
                'kondisi_berat' => $row['berat']
            ];
            Sapras::create($attributes);
        }
    }
}
