<?php

namespace App\Imports;

use App\Models\Ptk;
use App\Models\Sekolah;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class PtkImport implements ToCollection, WithHeadingRow
{
    /**
     * @param Collection $collection
     */
    public function collection(Collection $rows)
    {

        $sekolah = Sekolah::find(auth()->user()->sekolah_id);

        foreach ($rows as $row) {


            $attributes = [
                'nip' => $row['nip'],
                'nama' => $row['nama'],
                'tempat_lahir' => $row['tempat_lahir'],
                'tanggal_lahir' => $row['tanggal_lahir'],
                'alamat' => $row['alamat'],
                'telepon' => $row['telepon'],
                'email' => $row['email'],
                'status_pegawai_id' => $row['status_pegawai'],
                'pendidikan_id' => $row['pendidikan'],
                'jabatan_id' => $row['jabatan']
            ];

            $attributes['sekolah_id'] = $sekolah->id;

            if ($ptk = $sekolah->ptk->where('nip', $attributes['nip'])->first()) {

                $ptk->update($attributes);
            } else {

                Ptk::create($attributes);
            }
        }
    }
}
