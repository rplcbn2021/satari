<?php

namespace App\Imports;

use App\Models\Jenjang;
use App\Models\OldUser;
use App\Models\Sekolah;
use App\Models\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class SekolahImport implements ToCollection, WithHeadingRow
{
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) {

            $jenjang_id = 0;
            $kategori = 0;

            switch ($row['jenjang']) {
                case 'SMA':
                    $jenjang_id = 1;
                    break;
                case 'SMK':
                    $jenjang_id = 2;
                    break;
                case 'SLB':
                    $jenjang_id = 3;
            }

            switch ($row['kategori']) {
                case 'SWASTA':
                    $kategori = 1;
                    break;

                default:
                    $kategori = 2;
                    break;
            }

            $sekolah = Sekolah::create([
                'kecamatan_id' => $row['kecamatan_id'],
                'npsn' => $row['npsn'],
                'nama' => $row['nama_sekolah'],
                'alamat' => $row['alamat'],
                'jenjang_id' => $jenjang_id,
                'kategori_sekolah_id' => $kategori
            ]);
            $password1 = Str::random(8);
            $password2 = Str::random(8);

            $userOperator = User::create([
                'username' => 'operator' . $sekolah->npsn,
                'password' => Hash::make($password1),
                'sekolah_id' => $sekolah->id,
                'hak_akses_id' => 1,
                'remember_token' => Str::random(10)
            ]);

            $userKepsek = User::create([
                'username' => 'kepsek' . $sekolah->npsn,
                'password' => Hash::make($password2),
                'sekolah_id' => $sekolah->id,
                'hak_akses_id' => 2,
                'remember_token' => Str::random(10)
            ]);

            OldUser::create([
                'users_id' => $userOperator->id,
                'username' => $userOperator->username,
                'first_password' => $password1
            ]);

            OldUser::create([
                'users_id' => $userKepsek->id,
                'username' => $userKepsek->username,
                'first_password' => $password2
            ]);
        }
    }
}
