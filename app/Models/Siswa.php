<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    use HasFactory;
    protected $table = 'siswa';
    protected $guarded = [];

    public function rombel()
    {
        return $this->belongsTo(Rombel::class);
    }

    public function sekolah()
    {
        return $this->belongsTo(Sekolah::class);
    }
}
